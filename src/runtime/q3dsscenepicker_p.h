/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef Q3DSSCENEPICKER_H
#define Q3DSSCENEPICKER_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//


#include <QtCore/QObject>
#include <QtCore/QSize>
#include <QtCore/QQueue>
#include <QtCore/QPoint>
#include <Qt3DRender/QRayCaster>

#include "q3dsruntimeglobal_p.h"

QT_BEGIN_NAMESPACE

class Q3DSSceneManager;
class Q3DSGraphObject;
class Q3DSLayerNode;
class Q3DSInputManager;

class Q3DSV_PRIVATE_EXPORT Q3DSScenePicker : public QObject
{
    Q_OBJECT
public:
    enum PickState {
        Unqueued = 0,
        Queued,
        Triggered,
        Ready,
        Failed
    };

    struct PickResult {
        Q3DSGraphObject *m_object = nullptr;
        qreal m_distance = 0.0;
    };

    Q3DSScenePicker(Q3DSSceneManager *manager);
    ~Q3DSScenePicker() override;

    QPoint pickPoint() const;
    Q3DSLayerNode *pickLayer() const;
    bool isHit() const;

    void pick(const QPoint &point, Q3DSLayerNode *targetLayer = nullptr);

    const QVector<PickResult> &pickResults() const { return m_pickResults; }
    void addPick(Q3DSGraphObject *object, qreal distance);

    PickState state() const { return m_pickState; }
    void setState(PickState state);
    const QHash<Q3DSLayerNode *, PickState> &layerStates() const { return m_layerStates; }
    void setLayerState(Q3DSLayerNode *layer, PickState state);
    void reset();

    Q_SIGNAL void ready();

private:

    QPoint m_point;
    Q3DSLayerNode *m_layer = nullptr;
    Q3DSInputManager *m_inputManager = nullptr;
    PickState m_pickState = Unqueued;
    QHash<Q3DSLayerNode *, PickState> m_layerStates;
    QVector<PickResult> m_pickResults;
};

QT_END_NAMESPACE

#endif // Q3DSSCENEPICKER_H
