/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "q3dscubicroots_p.h"
#include <cmath>

QT_BEGIN_NAMESPACE

namespace Q3DSAnimationUtils {

/**
 * Find the root/s to a quadratic equation.
 *
 * The equation is of the form: c[2]*x^2 + c[1]*x + c[0] = 0
 * Note that this will fail if c[2] is zero, or this is a linear equation.
 *
 * @param constants - The array of constants to the equation; see form above
 * @param solutions - The array of solutions to the equation
 * @return the number of solutions for the equation
 */
qint32 solveQuadric(const float (&constants)[4], float (&solutions)[3])
{
    qint32 results;

    /* normal form: x^2 + px + theQ = 0 */
    const float P = constants[1] / (2.0f * constants[2]);
    const float Q = constants[0] / constants[2];
    const float discriminant = P * P - Q;

    if (discriminant == 0.0f) {
        solutions[0] = -P;
        results = 1;
    } else if (discriminant > 0.0f) {
        const float squareRootD = std::sqrt(discriminant);
        solutions[0] = squareRootD - P;
        solutions[1] = -solutions[0];
        results = 2;
    } else { // (discriminant < 0.0f)
        solutions[0] = 0.0f;
        results = 0;
    }

    return results;
}

/**
 * Find the root/s to a cubic equation.
 *
 * The equation is of the form: c[3]*x^3 + c[2]*x^2 + c[1]*x + c[0] = 0
 * Note that this will fail if c[3] is zero, or this is a quadratic equation.
 *
 * @param constants - The array of constants to the equation; see form above
 * @param solutions - The array of solutions to the equation
 * @return the number of solutions for the equation
 */
qint32 solveCubic(const float (&constants)[4], float (&solutions)[3])
{
    qint32 results;

    /* normal form: x^3 + Ax^2 + Bx + C = 0 */
    const float A = constants[2] / constants[3];
    const float B = constants[1] / constants[3];
    const float C = constants[0] / constants[3];

    const float oneThird = (1.0f / 3.0f);
    const float oneThirdPi = (oneThird * float(M_PI));

    /*  substitute x = y - A/3 to eliminate quadric term: x^3 +px + q = 0 */
    const float oneThirdA = (oneThird * A);
    const float ASquared = A * A;
    const float P = oneThird * (-oneThird * ASquared + B);
    const float Q = 1.0f / 2.0f * (2.0f / 27.0f * A * ASquared - oneThirdA * B + C);

    /* use Cardano's formula */
    const float pCubed = P * P * P;
    const float discriminant = Q * Q + pCubed;

    if (almostZero(discriminant, 1e-4f)) {
        if (almostZero(Q)) /* one triple solution */ {
            solutions[0] = 0.0f;
            results = 1;
        } else /* one single and one double solution */ {
            const float U = std::cbrt(-Q);
            solutions[0] = 2.0f * U;
            solutions[1] = -U;
            results = 2;
        }
    } else if (discriminant < 0.0f) /* Casus irreducibilis: three real solutions */ {
        const float phi = oneThird * std::acos(-Q / std::sqrt(-pCubed));
        const float T = 2.0f * std::sqrt(-P);

        solutions[0] = T * std::cos(phi);
        solutions[1] = -T * std::cos(phi + oneThirdPi);
        solutions[2] = -T * std::cos(phi - oneThirdPi);
        results = 3;
    } else /* one real solution */ {
        const float squareRootD = std::sqrt(discriminant);
        const float U = std::cbrt(squareRootD - Q);
        const float V = -std::cbrt(squareRootD + Q);

        solutions[0] = U + V;
        results = 1;
    }

    /* resubstitute */
    const float substitute = oneThirdA;
    for (qint32 i = 0; i < results; ++i)
        solutions[i] -= substitute;

    return results;
}

};

QT_END_NAMESPACE
