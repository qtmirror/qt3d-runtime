/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef Q3DSANIMATOR_P_H
#define Q3DSANIMATOR_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <QtCore/qglobal.h>
#include "utils/q3dsbeziereval_p.h"
#include "q3dsuippresentation_p.h"

#include <QVector>
#include <QVector4D>

QT_BEGIN_NAMESPACE

class Q3DSGraphObject;

struct Q3DSAnimationData
{
    enum class DataType : quint8
    {
        Component,
        KeyFrame,
        Target,
    };
    enum class Component : quint8 {
        X,
        Y,
        Z,
    };
    enum ComponentFlags : quint8
    {
        None,
        Dynamic = 0x1,
        Disabled = 0x2
    };

    struct ComponentData {
        mutable float value;
        quint16 size;
        Component component;
        ComponentFlags componentFlags;
    };

    using KeyFrameData = Q3DSAnimationUtils::KeyFrame;

    struct TargetData {
        Q3DSGraphObject *target;
        QVariant::Type type;
        qint16 pid;
        qint8 changeFlags;
    };

    union {
        ComponentData componentData;
        KeyFrameData keyFrameData;
        TargetData targetData;
    };

    DataType type;
};

Q_DECLARE_TYPEINFO(Q3DSAnimationData, Q_MOVABLE_TYPE);
Q_DECLARE_TYPEINFO(Q3DSAnimationData::KeyFrameData, Q_MOVABLE_TYPE);

struct Q3DSDirtyAnimationValue
{
    const Q3DSAnimationData::TargetData *target;
    QVector3D value;
};

Q_DECLARE_TYPEINFO(Q3DSDirtyAnimationValue, Q_MOVABLE_TYPE);

struct Q3DSAnimator
{
    using PlayMode = Q3DSSlide::PlayMode;

    // Time values are in seconds
    float rate  = 1.0f;
    float localTime = 0.0f;
    float duration = 0.0f;
    quint8 loopCount = 0; // We don't support setting an actuall loop count in the editor, so this is not implemented
    bool active = false;
    PlayMode playMode = PlayMode::StopAtEnd;

    using AnimationDataList = QVector<Q3DSAnimationData>;
    AnimationDataList animationDataList;

    using DirtyPropertyList = QVector<Q3DSDirtyAnimationValue>;
    DirtyPropertyList dirtyList;

    using EosCallback = std::function<void()>;
    EosCallback eosCallback;
    using TimeChangeCallback = std::function<void(float /* seconds */)>;
    TimeChangeCallback timeChangeCallback;

    void goToTime(float time, bool sync = false);

    // Note that the animator is not self-driven, so advance needs to be called periodically,
    // i.e., by attaching it to the frame updater...
    void advance(float dt);
};

bool evaluateAnimation(const Q3DSAnimator::AnimationDataList::const_iterator &cbegin,
                       const Q3DSAnimator::AnimationDataList::const_iterator &cend,
                       float time,
                       float *value);
void syncDirtyProperties(const Q3DSAnimator::DirtyPropertyList &dirtyProperties);
void evaluateNodesAt(Q3DSAnimator::AnimationDataList::const_iterator cit,
                     const Q3DSAnimator::AnimationDataList::const_iterator cend,
                     float time,
                     Q3DSAnimator::DirtyPropertyList *dirtyList,
                     bool sync = false);


// TODO: Declare movable

QT_END_NAMESPACE

#endif // Q3DSANIMATOR_P_H
