/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "q3dsanimator_p.h"

#include <q3dsuippresentation_p.h>
#include <q3dslogging_p.h>
#include <q3dsslideplayer_p.h>

QT_BEGIN_NAMESPACE

bool evaluateAnimation(const Q3DSAnimator::AnimationDataList::const_iterator &cbegin,
                       const Q3DSAnimator::AnimationDataList::const_iterator &cend,
                       float time,
                       float *value)
{
    Q_ASSERT(value);
    const float oldValue = *value;
    const int size = int(cend - cbegin) + 1;
    if (size < 2) {
        if (size == 0)
            return false;
        *value = cbegin->keyFrameData.value;
    } else {
        if (time >= cend->keyFrameData.time) {
            *value = cend->keyFrameData.value;
        } else if (time <= cbegin->keyFrameData.time) {
            *value = cbegin->keyFrameData.value;
        } else {
            const auto foundIt = std::find_if(cbegin, cend, [time](const Q3DSAnimationData &data) { return (data.keyFrameData.time >= time); });
            const auto previous = (foundIt != cbegin) ? (foundIt - 1) : foundIt;
            Q_ASSERT(foundIt->type == Q3DSAnimationData::DataType::KeyFrame && previous->type == Q3DSAnimationData::DataType::KeyFrame);
            *value = Q3DSAnimationUtils::evaluateBezierKeyframe(time, (*previous).keyFrameData, (*foundIt).keyFrameData);
        }
    }
    return !qFuzzyCompare(*value, oldValue);
}

void syncDirtyProperties(const Q3DSAnimator::DirtyPropertyList &dirtyProperties)
{
    Q3DSGraphObject *target;
    qint8 changeFlags;
    qint16 pid;
    QVariant::Type type;
    for (const auto &dirtyProperty : dirtyProperties) {
        target = dirtyProperty.target->target;
        Q_ASSERT(target);
        pid = dirtyProperty.target->pid;
        type = dirtyProperty.target->type;
        if (Q_UNLIKELY(pid == -1 || type == QVariant::Invalid)) {
            qCWarning(lcAnim, "Property update failed for id %d and with type %d", pid, type);
            continue;
        }

        const auto &vec3 = dirtyProperty.value;
        changeFlags = dirtyProperty.target->changeFlags;
        static const auto toTargetVariant = [](QVariant::Type type, const QVector3D &v) {
            switch (int(type)) {
            case QVariant::Double:
                Q_FALLTHROUGH();
            case QMetaType::Float:
                return QVariant::fromValue(v.x());
            case QVariant::Vector2D:
                return QVariant::fromValue(v.toVector2D());
            case QVariant::Vector3D:
                return QVariant::fromValue(v);
            case QVariant::Color:
            {
                // stabilize values
                const float r = qBound(0.0f, v.x(), 1.0f);
                const float g = qBound(0.0f, v.y(), 1.0f);
                const float b = qBound(0.0f, v.z(), 1.0f);
                return QVariant::fromValue(QColor::fromRgbF(qreal(r), qreal(g), qreal(b)));
            }
            default:
                return QVariant();
            }
        };

        const QVariant &variant = toTargetVariant(type, vec3);
        if (Q_UNLIKELY(!variant.isValid())) {
            qCWarning(lcAnim, "The unsupported type %d attempted used on %s", type, target->id().constData());
            continue;
        }

        if (Q_UNLIKELY(!Q3DSGraphObject::writeProperty(target, pid, variant))) {
            qCWarning(lcAnim, "Failed to write property %d on %s", pid, target->id().constData());
            continue;
        }
        target->notifyPropertyChanges(changeFlags, QSet<QString>());
    }
}

void evaluateNodesAt(Q3DSAnimator::AnimationDataList::const_iterator cit,
                     const Q3DSAnimator::AnimationDataList::const_iterator cend,
                     float time,
                     Q3DSAnimator::DirtyPropertyList *dirtyList,
                     bool sync)
{
    QVector3D vec3;
    // If sync is set to true we mark all as dirty to make sure the target property and the
    // animation node values are in sync.
    bool changed = sync;
    while (cit != cend) {
        if (cit->type == Q3DSAnimationData::DataType::Component) {
            const auto &componentData = cit->componentData;
            const auto &component = componentData.component;

            const auto cKfBegin = cit + 1;
            const auto cKfEnd = cKfBegin + componentData.size - 1;
            Q_ASSERT(cKfBegin->type == Q3DSAnimationData::DataType::KeyFrame
                     && cKfEnd->type == Q3DSAnimationData::DataType::KeyFrame);
            // Don't evaluate if the component is marked as disabled
            if (!(componentData.componentFlags & Q3DSAnimationData::ComponentFlags::Disabled)) {
                changed |= evaluateAnimation(cKfBegin, cKfEnd, time, &componentData.value);
                vec3[static_cast<int>(component)] = componentData.value;
            } else {
                changed = false;
            }
            cit = cKfEnd + 1; // Next should be either a component or a target (last kf + 1)
            Q_ASSERT(cit->type == Q3DSAnimationData::DataType::Component
                     || cit->type == Q3DSAnimationData::DataType::Target);
        }

        if (cit->type == Q3DSAnimationData::DataType::Target) {
            if (changed)
                dirtyList->push_back({&cit->targetData, std::move(vec3)});
            changed = sync; // reset the changed value
            ++cit;
        }

        Q_ASSERT(cit == cend || cit->type == Q3DSAnimationData::DataType::Component);
    }
}

void Q3DSAnimator::goToTime(float time, bool sync)
{
    if (time < 0.0f || time > duration)
        return;

    localTime = time;

    dirtyList.clear();
    evaluateNodesAt(animationDataList.cbegin(), animationDataList.cend(), localTime, &dirtyList, sync);
    syncDirtyProperties(dirtyList);

    if (timeChangeCallback)
        timeChangeCallback(localTime);
}

void Q3DSAnimator::advance(float dt)
{
    if (!active) {
        // We still need to make sure that object visibility is handled, so send a time update.
        if (timeChangeCallback)
            timeChangeCallback(localTime);
        return;
    }

    const float newTime = qBound(0.0f, localTime + (dt * rate), duration);

    bool eos = false;
    bool resetLocalTime = false;
    // 1. +rate && newTime == duration => EOS
    // 2. -rate && newTime == 0.0f => EOS
    // 3. !rate => no-op
    // Note that we only notify aboud EOS when we're not looping/pingponging.
    if (((rate > 0.0f) && qFuzzyCompare(newTime, duration)) || ((rate < 0.0f) && qFuzzyCompare(newTime, 0.0f))) {
        switch (playMode) {
        case PlayMode::PlayThroughTo:
            // We don't handle slide changes here, as that's a foreign concept,
            // so this needs to be handled by whoever registered for the eos callback.
            Q_FALLTHROUGH();
        case PlayMode::StopAtEnd:
            eos = true;
            break;
        case PlayMode::Looping:
            resetLocalTime = true;
            break;
        case PlayMode::Ping:
            if (loopCount == 0) {
                rate = (rate > 0.0f) ? -rate : -rate;
                loopCount = 1;
            } else {
                loopCount = 0;
                eos = true;
            }
            break;
        case PlayMode::PingPong:
            rate = -rate;
            break;
        }
    }

    if (!eos && qFuzzyCompare(newTime, localTime))
        return;

    goToTime(newTime);

    // If we're looping we need to reset the local time now.
    if (resetLocalTime)
        localTime = (rate > 0.0f) ? 0.0f : duration;

    if (eos && eosCallback)
        eosCallback();
}

QT_END_NAMESPACE
