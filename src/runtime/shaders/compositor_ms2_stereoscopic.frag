#version 310 es
precision highp float;

in vec2 texCoord;

uniform highp sampler2DMS texLeft;
uniform highp sampler2DMS texRight;
uniform highp int stereoMode;

out vec4 fragColor;

void main()
{
    ivec2 tc = ivec2(floor(vec2(textureSize(texLeft)) * texCoord));
    vec4 cLeft = vec4(0.0);
    vec4 cRight = vec4(0.0);
    // Fetch from correct texture
    if (stereoMode == 1) {
        if (texCoord.y > 0.5)
            cLeft = texelFetch(texLeft, tc, 0) + texelFetch(texLeft, tc, 1);
        else
            cRight = texelFetch(texRight, tc, 0) + texelFetch(texRight, tc, 1);
    } else if (stereoMode == 2) {
        if (texCoord.x < 0.5)
            cLeft = texelFetch(texLeft, tc, 0) + texelFetch(texLeft, tc, 1);
        else
            cRight = texelFetch(texRight, tc, 0) + texelFetch(texRight, tc, 1);
    } else {
        cLeft = texelFetch(texLeft, tc, 0) + texelFetch(texLeft, tc, 1);
        cRight = texelFetch(texRight, tc, 0) + texelFetch(texRight, tc, 1);
    }

    cLeft /= 2.0;
    cRight /= 2.0;
    // This discard, while not necessarily ideal for some GPUs, is necessary to
    // get correct results with certain layer blend modes for example.
    if (cLeft.a == 0.0 && cRight.a == 0.0)
        discard;
    if (stereoMode == 3) {
        cLeft.g = 0.0;
        cLeft.b = 0.0;
        cRight.r = 0.0;
    } else if (stereoMode == 4) {
        cLeft.r = 0.0;
        cLeft.b = 0.0;
        cRight.g = 0.0;
    }
    fragColor = cLeft + cRight;
}
