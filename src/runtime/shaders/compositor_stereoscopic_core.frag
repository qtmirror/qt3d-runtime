#version 330 core

in vec2 texCoord;

uniform sampler2D texLeft;
uniform sampler2D texRight;
uniform int stereoMode;

out vec4 fragColor;

void main()
{
    vec4 cLeft = vec4(0.0);
    vec4 cRight = vec4(0.0);
    // Fetch from correct texture
    if (stereoMode == 1) {
        if (texCoord.y > 0.5)
            cLeft = texture(texLeft, texCoord);
        else
            cRight = texture(texRight, texCoord);
    } else if (stereoMode == 2) {
        if (texCoord.x < 0.5)
            cLeft = texture(texLeft, texCoord);
        else
            cRight = texture(texRight, texCoord);
    } else {
        cLeft = texture(texLeft, texCoord);
        cRight = texture(texRight, texCoord);
    }

    // This discard, while not necessarily ideal for some GPUs, is necessary to
    // get correct results with certain layer blend modes for example.
    if (cLeft.a == 0.0 && cRight.a == 0.0)
        discard;
    if (stereoMode == 3) {
        cLeft.g = 0.0;
        cLeft.b = 0.0;
        cRight.r = 0.0;
    } else if (stereoMode == 4) {
        cLeft.r = 0.0;
        cLeft.b = 0.0;
        cRight.g = 0.0;
    }
    fragColor = cLeft + cRight;
}
