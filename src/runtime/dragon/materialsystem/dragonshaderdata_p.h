/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QT3DRENDER_DRAGON_SHADERDATA_P_H
#define QT3DRENDER_DRAGON_SHADERDATA_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <private/dragonbackendnode_p.h>

#include <Qt3DCore/private/matrix4x4_p.h>

#include <Qt3DRender/qshaderdata.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {

namespace Dragon {

// TODO: Dragon doesn't currently use anything from this class.
// So consider refactoring / removing.
class ShaderData : public BackendNode
{
public:
    enum TransformType {
        NoTransform = -1,
        ModelToEye = 0,
        ModelToWorld,
        ModelToWorldDirection
    };

    QHash<QString, QVariant> properties() const { return m_originalProperties; }

    // Called by FramePreparationJob
    void updateWorldTransform(const Matrix4x4 &worldMatrix);

    TransformType propertyTransformType(const QString &name) const;
    QVariant getTransformedProperty(const QString &name, const Matrix4x4 &viewMatrix);

    void initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change) override;
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e) override;

protected:

    PropertyReaderInterfacePtr m_propertyReader;

    // 1 to 1 match with frontend properties, modified only by sceneChangeEvent
    QHash<QString, QVariant> m_originalProperties;

    // Contains properties thar are of type ShaderData
    QHash<QString, QVariant> m_nestedShaderDataProperties;

    // Contains property that are defined like: postionTransformed: ModelToEye
    QHash<QString, TransformType> m_transformedProperties;

    Matrix4x4 m_worldMatrix;

    void clearUpdatedProperties();
    ShaderData *lookupResource(Qt3DCore::QNodeId id);
};

} // namespace Dragon

} // namespace Qt3DRender

QT_END_NAMESPACE

//Q_DECLARE_METATYPE(Qt3DRender::Render::ShaderData*) // LCOV_EXCL_LINE

#endif // QT3DRENDER_DRAGON_SHADERDATA_P_H
