/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QT3DRENDER_DRAGON_TECHNIQUE_H
#define QT3DRENDER_DRAGON_TECHNIQUE_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <private/q3dsruntimeglobal_p.h>
#include <private/dragonbackendnode_p.h>
#include <private/dragonparameterpack_p.h>
#include <Qt3DRender/private/qgraphicsapifilter_p.h>

#include <QVector>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {

class QTechnique;
class QParameter;
class QGraphicsApiFilter;
class QFilterKey;
class QRenderPass;

namespace Dragon {

class TechniqueManager;

class Technique : public BackendNode
{
public:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e) override;
    QVector<Qt3DCore::QNodeId> parameters() const;

    QVector<Qt3DCore::QNodeId> filterKeys() const;
    QVector<Qt3DCore::QNodeId> renderPasses() const;
    GraphicsApiFilterData graphicsApiFilter() const;

    void initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change) final;

private:
    void appendRenderPass(Qt3DCore::QNodeId renderPassId);
    void removeRenderPass(Qt3DCore::QNodeId renderPassId);

    void appendFilterKey(Qt3DCore::QNodeId criterionId);
    void removeFilterKey(Qt3DCore::QNodeId criterionId);

    GraphicsApiFilterData m_graphicsApiFilterData;
    Dragon::ParameterPack m_parameterPack;
    QVector<Qt3DCore::QNodeId> m_filterKeyList;
    QVector<Qt3DCore::QNodeId> m_renderPasses;
};

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // QT3DRENDER_DRAGON_TECHNIQUE_H
