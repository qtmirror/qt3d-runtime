/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragontexturejobs_p.h"

#include <private/dragonjobs_common_p.h>

#include <private/dragontexture_p.h>

#include <Qt3DRender/qtexturegenerator.h>

#include <QtConcurrentRun>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

// TODO at this point, this function might as well not return anything but the vector of futures
// and not take anything back in again
LoadedTextureImages loadTextureImages(LoadedTextureImages loadedTextureImages,
                                      const ValueContainer<TextureImage> &textureImages)
{
    loadedTextureImages.reset();

    if (!textureImages.anythingDirty())
        return loadedTextureImages;

    auto generateTexture = [](const QNodeId &id, const Immutable<TextureImage> &textureImage) {
        Q_UNUSED(id);
        LoadedTextureImage info;
        info.image = textureImage;
        info.data = (*textureImage->generator)();
        return info;
    };

    loadedTextureImages = synchronizeKeys(std::move(loadedTextureImages),
                                          textureImages,
                                          generateTexture);

    return loadedTextureImages;
}

LoadedTextures loadTextures(LoadedTextures loadedTextures, const ValueContainer<Texture> &textures,
                            const LoadedTextureImages &loadedImages)
{
    loadedTextures.reset();

    if (!textures.anythingDirty() && !loadedImages.anythingDirty())
        return loadedTextures;

    auto generateTexture = [&loadedImages](const QNodeId &id, const Immutable<Texture> &texture) {
        LoadedTexture loadedTexture;
        loadedTexture.texture = texture;

        for (const auto &loadedImageId : loadedImages.keys()) {
            // TODO avoid lookup
            const auto &loadedImage = loadedImages[loadedImageId];
            if (loadedImage->image->parentId == id) {
                loadedTexture.images.push_back(loadedImage);
            }
        }

        if (texture->generator == nullptr)
            return loadedTexture;

        if (texture->properties.target != QAbstractTexture::TargetAutomatic)
            qWarning() << "[Qt3DRender::GLTexture] When a texture provides a generator, it's "
                          "target is expected to be TargetAutomatic";

        loadedTexture.data = (*texture->generator)();

        return loadedTexture;
    };

    // TODO consider reusing existing when generators are shared, like we do with texture images
    loadedTextures = synchronizeKeys(std::move(loadedTextures), textures, generateTexture);

    // TODO this is not the cleanest approach to finding dirty texture images that affect our
    // texture. Consider storing a hash of textures using different images, or implementing
    // more sophisticated dependency tracking between different types of nodes.
    const auto &changes = loadedImages.changes();
    for (const auto &textureId : loadedTextures.keys()) {
        const auto loadedTextureImages = loadedTextures[textureId]->images;
        for (const auto &loadedTextureImage : loadedTextureImages) {
            const QNodeId textureImageId = loadedTextureImage->image->peerId();
            if (changes.contains(Change{Change::Action::Modified, textureImageId}) ||
                    changes.contains(Change{Change::Action::Created, textureImageId})) {
                loadedTextures[textureId] = generateTexture(textureId, textures[textureId]);
                loadedTextures.markDirty(textureId);
            }
        }
    }
    return loadedTextures;
}

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE
