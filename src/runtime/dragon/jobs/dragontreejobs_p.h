/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QT3DRENDER_DRAGON_TREEJOBS_H
#define QT3DRENDER_DRAGON_TREEJOBS_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <private/dragonjobs_common_p.h>
#include <private/dragontextureimage_p.h>
#include <private/dragonvaluecontainer_p.h>
#include <private/dragonnodetree_p.h>
#include <private/dragonframegraphnode_p.h>

#include <Qt3DCore/QNodeId>

#include <QStack>
#include <QThread>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

// TODO consider storing handles/values instead of keys to reduce lookups
struct InheritanceInfo
{
    using Key = Qt3DCore::QNodeId;

    // TODO consider adding an ancestor list that includes ourselves (useful to build render views)
    QVector<Key> ancestors;
    QVector<Key> descendants;

    void clear()
    {
        ancestors.clear();
        descendants.clear();
    }
};

struct TreeInfo
{
    using Key = Qt3DCore::QNodeId;
    ValueContainer<InheritanceInfo> nodes;
    QVector<Key> leafNodes; // ordered
    Key rootNode;
};

TreeInfo generateInheritanceTable(TreeInfo inheritanceTable,
                                  const NodeTree::Nodes hierarchy,
                                  const ValueContainer<FrameGraphNode> nodes,
                                  Qt3DCore::QNodeId rootEntityId);

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // QT3DRENDER_DRAGON_TREEJOBS_H
