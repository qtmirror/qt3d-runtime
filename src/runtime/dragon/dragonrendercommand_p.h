/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DRAGONRENDERCOMMAND_P_H
#define DRAGONRENDERCOMMAND_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <private/dragonimmutable_p.h>
#include <private/dragonshaderparameterpack_p.h>
#include <private/dragonrenderstateset_p.h>

#include <Qt3DRender/qgeometryrenderer.h>

QT_BEGIN_NAMESPACE
namespace Qt3DRender {
namespace Dragon {

class Shader;
struct LoadedBuffer;
class Geometry;
class GeometryRenderer;
class Material;
class Parameter;
class Entity;

// Map from nameId -> parameter
using ParameterNameToIdMap = QHash<int, Qt3DCore::QNodeId>;
using ParameterNameToParameterMap = QHash<int, Immutable<Parameter>>;

class RenderCommand
{
public:
    Immutable<Shader> m_shader;

    Immutable<GeometryRenderer> m_geometryRenderer;
    Immutable<Geometry> m_geometry;
    Immutable<Material> m_material;
    Immutable<Entity> m_entity;

    ParameterNameToIdMap m_parameterIds; // added in Dragon

    // TODO add back render state set
    Immutable<RenderStateSet> m_renderStateSet;

    float m_depth = 0.0f;
    // TODO: Currently only m_depth based sorting is supported in dragonrenderviewjobs.
    // If StateChangeCost or Material sorting will not be supported, these can be removed.
    int m_changeCost = 0;
    uint m_shaderDna = 0;

    enum CommandType { Draw, Compute };

    CommandType m_type = Draw;

    QHash<QString, int> m_fragOutputs;
};

}
}
QT_END_NAMESPACE

#endif // DRAGONRENDERCOMMAND_P_H
