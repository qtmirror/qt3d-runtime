/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragontransform_p.h"

#include <Qt3DCore/qtransform.h>
#include <Qt3DCore/private/qtransform_p.h>

#include <Qt3DCore/QComponentAddedChangePtr>
#include <Qt3DCore/QNodeCreatedChangeBasePtr>
#include <Qt3DCore/QNodeId>
#include <Qt3DCore/QPropertyUpdatedChangePtr>

QT_BEGIN_NAMESPACE
namespace Qt3DRender {
namespace Dragon {

Transform::Transform() {}

void Transform::updateMatrix()
{
    QMatrix4x4 m;
    m.translate(m_translation);
    m.rotate(m_rotation);
    m.scale(m_scale);
    m_transformMatrix = Matrix4x4(m);

    markDirty();
}

void Transform::initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change)
{
    const auto typedChange =
        qSharedPointerCast<Qt3DCore::QNodeCreatedChange<Qt3DCore::QTransformData>>(change);
    const auto &data = typedChange->data;
    m_rotation = data.rotation;
    m_scale = data.scale;
    m_translation = data.translation;
    updateMatrix();
}

void Transform::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    // TODO: Flag the matrix as dirty and update all matrices batched in a job
    if (e->type() == Qt3DCore::PropertyUpdated) {
        const Qt3DCore::QPropertyUpdatedChangePtr &propertyChange =
            qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);
        if (propertyChange->propertyName() == QByteArrayLiteral("scale3D")) {
            m_scale = propertyChange->value().value<QVector3D>();
            updateMatrix();
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("rotation")) {
            m_rotation = propertyChange->value().value<QQuaternion>();
            updateMatrix();
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("translation")) {
            m_translation = propertyChange->value().value<QVector3D>();
            updateMatrix();
            markDirty();
        }
    }
}

} // namespace Dragon
} // namespace Qt3DRender
QT_END_NAMESPACE
