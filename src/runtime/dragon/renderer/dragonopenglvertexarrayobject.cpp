/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragonopenglvertexarrayobject_p.h"

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

GLVertexArrayObject::GLVertexArrayObject()
//    : m_ctx(nullptr)
    : m_specified(false)
    , m_supportsVao(false)
{}

void GLVertexArrayObject::bind()
{
    qDebug() << "WARNING: Tried binding not implemented GLVertexArrayObject";
//    Q_ASSERT(m_ctx);
//    if (m_supportsVao) {
//        Q_ASSERT(!m_vao.isNull());
//        Q_ASSERT(m_vao->isCreated());
//        m_vao->bind();
//    } else {
//        // Unbind any other VAO that may have been bound and not released correctly
//        if (m_ctx->m_currentVAO != nullptr && m_ctx->m_currentVAO != this)
//            m_ctx->m_currentVAO->release();

//        m_ctx->m_currentVAO = this;
//        // We need to specify array and vertex attributes
//        for (const SubmissionContext::VAOVertexAttribute &attr : qAsConst(m_vertexAttributes))
//            m_ctx->enableAttribute(attr);
//        if (!m_indexAttribute.isNull())
//            m_ctx->bindGLBuffer(m_ctx->m_renderer->nodeManagers()->glBufferManager()->data(m_indexAttribute),
//                                GLBuffer::IndexBuffer);
//    }
}

void GLVertexArrayObject::release()
{
    qDebug() << "WARNING: Releasing vertex array object not implemented";
//    Q_ASSERT(m_ctx);
//    if (m_supportsVao) {
//        Q_ASSERT(!m_vao.isNull());
//        Q_ASSERT(m_vao->isCreated());
//        m_vao->release();
//    } else {
//        if (m_ctx->m_currentVAO == this) {
//            for (const SubmissionContext::VAOVertexAttribute &attr : qAsConst(m_vertexAttributes))
//                m_ctx->disableAttribute(attr);
//            m_ctx->m_currentVAO = nullptr;
//        }
//    }
}

// called from Render thread
//void OpenGLVertexArrayObject::create(SubmissionContext *ctx, const VAOIdentifier &key)
//{
//    QMutexLocker lock(&m_mutex);

//    Q_ASSERT(!m_ctx && !m_vao);

//    m_ctx = ctx;
//    m_supportsVao = m_ctx->supportsVAO();
//    if (m_supportsVao) {
//        m_vao.reset(new QOpenGLVertexArrayObject());
//        m_vao->create();
//    }
//    m_owners = key;
//}

// called from Render thread
void GLVertexArrayObject::destroy()
{
    qDebug() << "WARNING: Destroying vertex array object not implemented";
//    QMutexLocker locker(&m_mutex);

//    Q_ASSERT(m_ctx);
//    cleanup();
}

// called from job
bool GLVertexArrayObject::isAbandoned(GeometryManager *geomMgr, ShaderManager *shaderMgr)
{
    Q_UNUSED(geomMgr)
    Q_UNUSED(shaderMgr)
    qDebug() << "WARNING: isAbandoned vertex array object not implemented";
    return false;
//    QMutexLocker lock(&m_mutex);

//    if (!m_ctx)
//        return false;

//    const bool geometryExists = (geomMgr->data(m_owners.first) != nullptr);
//    const bool shaderExists = (shaderMgr->data(m_owners.second) != nullptr);

//    return !geometryExists || !shaderExists;
}

void GLVertexArrayObject::saveVertexAttribute(const VAOVertexAttribute &attr)
{
    // TODO is this the right place to do this? Could we verify earlier that we have no
    // duplicate locations?

    // Remove any vertexAttribute already at location
    for (auto i = m_vertexAttributes.size() - 1; i >= 0; --i) {
        if (m_vertexAttributes.at(i).location == attr.location) {
            m_vertexAttributes.removeAt(i);
            break;
        }
    }
    m_vertexAttributes.push_back(attr);
}


} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE
