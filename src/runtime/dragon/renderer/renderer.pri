INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/dragonopenglvertexarrayobject_p.h \
    $$PWD/dragonshaderparameterpack_p.h \
    $$PWD/dragonshadervariables_p.h \
    $$PWD/dragonuniform_p.h \
    $$PWD/dragondraw_p.h

SOURCES += \
    $$PWD/dragonopenglvertexarrayobject.cpp \
    $$PWD/dragonshaderparameterpack.cpp \
    $$PWD/dragonuniform.cpp \
    $$PWD/dragondraw.cpp
