/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragonmemorybarrier_p.h"

#include <Qt3DCore/qpropertyupdatedchange.h>

#include <Qt3DRender/private/qmemorybarrier_p.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {

namespace Dragon {

MemoryBarrier::MemoryBarrier()
    : FrameGraphNode(FrameGraphNode::MemoryBarrier)
    , m_waitOperations(QMemoryBarrier::None)
{
}

MemoryBarrier::~MemoryBarrier()
{
}

QMemoryBarrier::Operations MemoryBarrier::waitOperations() const
{
    return m_waitOperations;
}

void MemoryBarrier::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    if (e->type() == Qt3DCore::PropertyUpdated) {
        Qt3DCore::QPropertyUpdatedChangePtr propertyChange = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);
        if (propertyChange->propertyName() == QByteArrayLiteral("waitOperations")) {
            m_waitOperations = propertyChange->value().value<QMemoryBarrier::Operations>();
            markDirty();
        }
    }
    FrameGraphNode::sceneChangeEvent(e);
}

void MemoryBarrier::initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change)
{
    FrameGraphNode::initializeFromPeer(change);
    const auto typedChange = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<QMemoryBarrierData>>(change);
    const QMemoryBarrierData &data = typedChange->data;
    m_waitOperations = data.waitOperations;
}

} // Dragon

} // Qt3DRender

QT_END_NAMESPACE
