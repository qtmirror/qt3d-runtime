/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragonframegraphnode_p.h"

#include <Qt3DCore/qpropertynoderemovedchange.h>
#include <Qt3DCore/qpropertynodeaddedchange.h>
#include <Qt3DCore/qpropertyupdatedchange.h>
#include <Qt3DCore/qnodecreatedchange.h>
#include <Qt3DCore/qnodedestroyedchange.h>

#include <Qt3DRender/qframegraphnode.h>
#include <Qt3DRender/qframegraphnodecreatedchange.h>
#include <Qt3DRender/private/qframegraphnodecreatedchange_p.h>

#include <Qt3DCore/private/qscenechange_p.h>

// TODO only needed for DirtyTreeInfo - consider moving to separate file
#include <private/dragontreejobs_p.h>
#include <private/qobject_p.h>

using namespace Qt3DCore;

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

FrameGraphNode::FrameGraphNode()
    : BackendNode()
    , m_nodeType(InvalidNodeType)
{
}

FrameGraphNode::FrameGraphNode(FrameGraphNodeType nodeType, Qt3DCore::QBackendNode::Mode mode)
    : BackendNode()
    , m_nodeType(nodeType)
{
    Q_UNUSED(mode)
}

FrameGraphNode::~FrameGraphNode()
{
}

void FrameGraphNode::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    BackendNode::sceneChangeEvent(e);
}

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE
