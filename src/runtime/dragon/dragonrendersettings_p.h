/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QT3DRENDER_DRAGON_RENDERSETTINGS_H
#define QT3DRENDER_DRAGON_RENDERSETTINGS_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <private/dragonbackendnode_p.h>
#include <private/dragonmapper_p.h>

#include <Qt3DRender/qrendersettings.h>
#include <Qt3DRender/qpickingsettings.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

class QDragonRenderAspect;

class Q_AUTOTEST_EXPORT RenderSettings : public Qt3DCore::QBackendNode
{
public:
    RenderSettings(NodeTree *nodeTree);

    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e) override;

    Qt3DCore::QNodeId activeFrameGraphID() const { return m_activeFrameGraph; }
    QRenderSettings::RenderPolicy renderPolicy() const { return m_renderPolicy; }
    QPickingSettings::PickMethod pickMethod() const { return m_pickMethod; }
    QPickingSettings::PickResultMode pickResultMode() const { return m_pickResultMode; }
    QPickingSettings::FaceOrientationPickingMode faceOrientationPickingMode() const { return m_faceOrientationPickingMode; }
    float pickWorldSpaceTolerance() const { return m_pickWorldSpaceTolerance; }

    // For unit test purposes
    void setActiveFrameGraphId(Qt3DCore::QNodeId frameGraphNodeId) { m_activeFrameGraph = frameGraphNodeId; }

private:
    void initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change) final;

    QRenderSettings::RenderPolicy m_renderPolicy;
    QPickingSettings::PickMethod m_pickMethod;
    QPickingSettings::PickResultMode m_pickResultMode;
    QPickingSettings::FaceOrientationPickingMode m_faceOrientationPickingMode;
    float m_pickWorldSpaceTolerance;
    Qt3DCore::QNodeId m_activeFrameGraph;
    NodeTree *m_nodeTree = nullptr;
};

class RenderSettingsFunctor : public Qt3DCore::QBackendNodeMapper
{
public:
    explicit RenderSettingsFunctor(QDragonRenderAspect *renderer, NodeTree *nodeTree);
    Qt3DCore::QBackendNode *create(const Qt3DCore::QNodeCreatedChangeBasePtr &change) const override;
    Qt3DCore::QBackendNode *get(Qt3DCore::QNodeId id) const override;
    void destroy(Qt3DCore::QNodeId id) const override;

private:
    QDragonRenderAspect *m_renderer = nullptr;
    NodeTree *m_nodeTree = nullptr;
};

} // namespace Render
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // QT3DRENDER_DRAGON_RENDERSETTINGS_H
