/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef BACKENDNODE_H
#define BACKENDNODE_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "dragontrackingchangescontainer_p.h"
#include <Qt3DCore/QBackendNode>
#include <Qt3DStudioRuntime2/private/q3dsruntimeglobal_p.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

// TODO NodeContainer should have non-const access for the node mapper, but const-only access for
// everyone else Perhaps we could store non-const object, but only give out access to const objects?
// Keeping it non-const for now...
template <typename T>
using NodeContainer = TrackingChangesContainer<T *>;

// TODO consider adding an enabled property to this (since we already have this)
class Q3DSV_PRIVATE_EXPORT BackendNode
{
public:
    BackendNode() = default;
    virtual ~BackendNode();

    void setManager(AbstractContainer *manager);

    // TODO consider removing, this is a bit hacky
    // TODO perhaps we could just use the fact that we changed?
    void markDirty();
    void markDirty(Change::DirtyInfo info);
    Qt3DCore::QNodeId peerId() const;

    // TODO consider making private and protected by befriending DragonNodeFunctor and
    // make DragonNodeFunctor store Value<BackendNode> instead of Value<Backend>
    virtual void initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change)
    {
        Q_UNUSED(change);
    }
    virtual void sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
    {
        Q_UNUSED(e);
    }
    void setPeerId(const Qt3DCore::QNodeId &peerId);

    bool isEnabled() const
    {
        return m_enabled;
    }

    void setEnabled(bool enabled)
    {
        m_enabled = enabled;
    }

private:
    AbstractContainer *m_manager = nullptr;
    Qt3DCore::QNodeId m_peerId;
    bool m_enabled = true;
};

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // BACKENDNODE_H
