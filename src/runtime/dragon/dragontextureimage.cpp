/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragontextureimage_p.h"

#include <private/dragoncomparegenerators_p.h>

#include <Qt3DRender/qabstracttextureimage.h>
#include <Qt3DRender/private/qabstracttextureimage_p.h>

#include <Qt3DCore/QPropertyUpdatedChange>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

void TextureImage::initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change)
{
    const auto typedChange = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<QAbstractTextureImageData>>(change);
    const auto &data = typedChange->data;
    mipLevel = data.mipLevel;
    layer = data.layer;
    face = data.face;
    generator = data.generator;

    // TODO add job that uses this to set texture image on parent automatically
    parentId = change->parentId();
}

void TextureImage::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    QPropertyUpdatedChangePtr propertyChange = qSharedPointerCast<QPropertyUpdatedChange>(e);

    if (e->type() == PropertyUpdated) {
        if (propertyChange->propertyName() == QByteArrayLiteral("layer")) {
            layer = propertyChange->value().toInt();
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("mipLevel")) {
            mipLevel = propertyChange->value().toInt();
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("face")) {
            face = static_cast<QAbstractTexture::CubeMapFace>(propertyChange->value().toInt());
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("dataGenerator")) {
            generator = propertyChange->value().value<QTextureImageDataGeneratorPtr>();
            markDirty();
        }
    }

    markDirty();
    BackendNode::sceneChangeEvent(e);
}

bool operator ==(const TextureImage &a, const TextureImage &b)
{
    return compareGenerators(a.generator, b.generator) && a.face == b.face && a.layer == b.layer && a.mipLevel == b.mipLevel;
}

bool operator ==(const LoadedTextureImage &a, const LoadedTextureImage &b)
{
    return a.image == b.image;
}

}
}
QT_END_NAMESPACE
