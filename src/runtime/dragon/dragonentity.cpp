/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragonentity_p.h"

#include <Qt3DCore/private/qentity_p.h>

#include <Qt3DRender/qcameralens.h>
#include <Qt3DRender/qgeometryrenderer.h>
#include <Qt3DRender/qlayer.h>
#include <Qt3DRender/qmaterial.h>

#include <Qt3DCore/qcomponentaddedchange.h>
#include <Qt3DCore/qcomponentremovedchange.h>
#include <Qt3DCore/qnodecreatedchange.h>
#include <Qt3DCore/qpropertyupdatedchange.h>
#include <Qt3DCore/qtransform.h>

#include <Qt3DCore/qpropertynodeaddedchange.h>
#include <Qt3DCore/qpropertynoderemovedchange.h>

#include <private/dragontreejobs_p.h>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

Entity::Entity() {}

void Entity::initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change)
{
    const auto typedChange = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<Qt3DCore::QEntityData>>(
        change);
    const auto &data = typedChange->data;
    theId = change->subjectId();

    // Note this is *not* the parentId as that is the ID of the parent QNode, which is not
    // necessarily the same as the parent QEntity (which may be further up the tree).
    m_parentEntityId = data.parentEntityId;
    for (const auto &idAndType : qAsConst(data.componentIdsAndTypes))
        addComponent(idAndType);
}

void Entity::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    Q_UNUSED(e);

    // TODO we are missing some cases here from the original Qt3DRender::Entity

    switch (e->type()) {
    case ComponentAdded: {
        QComponentAddedChangePtr change = qSharedPointerCast<QComponentAddedChange>(e);
        const auto componentIdAndType = QNodeIdTypePair(change->componentId(),
                                                        change->componentMetaObject());
        markDirty();
        addComponent(componentIdAndType);
        break;
    }
    case ComponentRemoved: {
        QComponentRemovedChangePtr change = qSharedPointerCast<QComponentRemovedChange>(e);
        removeComponent(change->componentId());
        markDirty();
        break;
    }
    default:
        break;
    }
}

void Entity::addComponent(Qt3DCore::QNodeIdTypePair idAndType)
{
    // The backend element is always created when this method is called
    // If that's not the case something has gone wrong
    const auto type = idAndType.type;
    const auto id = idAndType.id;

    if (type->inherits(&Qt3DCore::QTransform::staticMetaObject)) {
        m_transformComponent = id;
    } else if (type->inherits(&QCameraLens::staticMetaObject)) {
        m_cameraLensComponent = id;
    } else if (type->inherits(&QLayer::staticMetaObject)) {
        m_layerComponents.append(id);
    } else if (type->inherits(&QMaterial::staticMetaObject)) {
        m_materialComponent = id;
    } else if (type->inherits(&QGeometryRenderer::staticMetaObject)) {
        m_geometryRendererComponent = id;
        //        m_boundingDirty = true;
    }

    // TODO consider adding these components back
    //    } else if (type->inherits(&QObjectPicker::staticMetaObject)) {
    //        m_objectPickerComponent = id;
    //    }
    //    } else if (type->inherits(&QLevelOfDetail::staticMetaObject)) {
    //        m_levelOfDetailComponents.append(id);
    //    } else if (type->inherits(&QRayCaster::staticMetaObject)) {
    //        m_rayCasterComponents.append(id);
    //    } else if (type->inherits(&QScreenRayCaster::staticMetaObject)) {
    //        m_rayCasterComponents.append(id);
    //    } else if (type->inherits(&QAbstractLight::staticMetaObject)) { //
    //        QAbstractLight subclasses QShaderData
    //                m_lightComponents.append(id);
    //    } else if (type->inherits(&QEnvironmentLight::staticMetaObject)) {
    //        m_environmentLightComponents.append(id);
    //    } else if (type->inherits(&QShaderData::staticMetaObject)) {
    //        m_shaderDataComponents.append(id);
    //    } else if (type->inherits(&QBoundingVolumeDebug::staticMetaObject))
    //        {
    //        m_boundingVolumeDebugComponent = id;
    //        }
    //        else if (type->inherits(&QComputeCommand::staticMetaObject)) { m_computeComponent =
    //        id; } else if (type->inherits(&QArmature::staticMetaObject)) { m_armatureComponent =
    //        id; }
    markDirty();
}

void Entity::removeComponent(QNodeId nodeId)
{
    if (m_transformComponent == nodeId) {
        m_transformComponent = QNodeId();
    } else if (m_cameraLensComponent == nodeId) {
        m_cameraLensComponent = QNodeId();
    } else if (m_layerComponents.contains(nodeId)) {
        m_layerComponents.removeAll(nodeId);
    } else if (m_levelOfDetailComponents.contains(nodeId)) {
        m_levelOfDetailComponents.removeAll(nodeId);
    } else if (m_rayCasterComponents.contains(nodeId)) {
        m_rayCasterComponents.removeAll(nodeId);
    } else if (m_materialComponent == nodeId) {
        m_materialComponent = QNodeId();
    } else if (m_shaderDataComponents.contains(nodeId)) {
        m_shaderDataComponents.removeAll(nodeId);
    } else if (m_geometryRendererComponent == nodeId) {
        m_geometryRendererComponent = QNodeId();
     } else if (m_objectPickerComponent == nodeId) {
        m_objectPickerComponent = QNodeId();
    } else if (m_lightComponents.contains(nodeId)) {
        m_lightComponents.removeAll(nodeId);
    } else if (m_environmentLightComponents.contains(nodeId)) {
        m_environmentLightComponents.removeAll(nodeId);
    } else if (m_computeComponent == nodeId) {
        m_computeComponent = QNodeId();
    } else if (m_armatureComponent == nodeId) {
        m_armatureComponent = QNodeId();
    }
}

Qt3DCore::QNodeId Entity::parentId() const
{
    return m_parentEntityId;
}

Qt3DCore::QNodeId Entity::cameraLensComponent() const
{
    return m_cameraLensComponent;
}

} // namespace Dragon
} // namespace Qt3DRender
QT_END_NAMESPACE
