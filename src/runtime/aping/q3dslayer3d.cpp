/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "q3dslayer3d_p.h"
#include "q3dsobject3d_p.h"
#include "q3dsstudio3dengine_p.h"
#include "q3dslayer3dsgnode_p.h"
#include "q3dsengine_p.h"
#include <QImage>
#include <QQuickWindow>
#include <QSGTexture>
#include <QSGTextureProvider>

QT_BEGIN_NAMESPACE

/*!
    \qmltype Layer3D
    //! \instantiates Q3DSLayer3D
    \inqmlmodule QtStudio3D
    \ingroup 3dstudioruntime2
    \inherits Item
    \since Qt 3D Studio 2.1

    \internal

    \brief An item rendering the contents of a single Qt 3D Studio layer.
 */

class Q3DSLayerTextureProvider : public QSGTextureProvider
{
public:
    QSGTexture *texture() const override { return m_texture; }
    void setTexture(QSGTexture *t) {
        if (t != m_texture) {
            m_texture = t;
            emit textureChanged();
        }
    }
private:
    QSGTexture *m_texture = nullptr;
};

Q3DSLayer3D::Q3DSLayer3D(QQuickItem *parent)
    : QQuickItem(parent)
{
    setFlag(QQuickItem::ItemHasContents, true);

    setAcceptedMouseButtons(Qt::MouseButtonMask);
    // do not opt in for hover - nothing in the 3D world needs it yet
}

// The concept of slides is not exposed in the API. All objects are
// automatically added to an implicit slide.
Q3DSSlide *Q3DSLayer3D::slide() const
{
    return static_cast<Q3DSSlide *>(m_presentation->masterSlide()->firstChild());
}

Q3DSLayer3D::~Q3DSLayer3D()
{
    if (m_engine)
        m_engine->unregisterView(this);

    // Null out m_layer to prevent children from choking when ~QQuickItem
    // generates a parent-is-null change after we return from here. In
    // addition, null out m_object as well recursively since the whole
    // Q3DSGraphObject tree under m_layer3DS is going to be destroyed.
    for (QQuickItem *item : childItems()) {
       Q3DSObject3D *child = qobject_cast<Q3DSObject3D *>(item);
       if (child)
           child->invalidate(Q3DSObject3D::InvalidateLayer | Q3DSObject3D::InvalidateObject);
    }

    if (m_presentation) {
        Q3DSUipPresentation::forAllObjectsInSubTree(m_layer3DS, [this](Q3DSGraphObject *obj) {
            slide()->removeObject(obj);
        });
        // going away for good, so removeChildNode + unregister id
        m_presentation->unlinkObject(m_layer3DS);
        delete m_layer3DS; // also destroys all its children
    }

    delete m_textureProvider;
}

// Relying on updatePaintNode to keep the SG node up-to-date is not feasable
// since the Qt3D rendering of the frame - and so the retrieval of the texture
// IDs - is happening in beforeRendering, which happens after the sync step of
// the Quick scenegraph. From there we cannot dirty the item and it's too late
// anyway. Instead, the SG node has to be updated directly from
// Q3DSStudio3DEngine::Renderer.

QSGNode *Q3DSLayer3D::updatePaintNode(QSGNode *node, QQuickItem::UpdatePaintNodeData *)
{
    // render thread

    // Have a transparent dummy texture since the node must have a valid
    // texture but it is not known when the Engine::Renderer is able to set a
    // texture on it.
    const QSize dummySize(64, 64);
    if (!m_dummyTexture) {
        QImage img(dummySize, QImage::Format_ARGB32_Premultiplied);
        img.fill(Qt::transparent);
        m_dummyTexture = window()->createTextureFromImage(img);
    }

    Q3DSLayer3DSGNode *n = static_cast<Q3DSLayer3DSGNode *>(node);
    if (!n) {
        n = new Q3DSLayer3DSGNode;
        n->setRect(QRectF(0, 0, dummySize.width(), dummySize.height()));
        n->setTexture(m_dummyTexture);
        n->setOwnsTexture(true);
    }

    m_node = n;

    return n;
}

void Q3DSLayer3D::itemChange(QQuickItem::ItemChange change,
                             const QQuickItem::ItemChangeData &changeData)
{
    switch (change) {
    case ItemSceneChange:
        sync();
        break;

    default:
        break;
    }

    QQuickItem::itemChange(change, changeData);
}

void Q3DSLayer3D::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    QQuickItem::geometryChanged(newGeometry, oldGeometry);
    if (!newGeometry.isEmpty() && newGeometry.size() != oldGeometry.size())
        sync();
}

void Q3DSLayer3D::componentComplete()
{
    QQuickItem::componentComplete();
    sync();
}

QObject *Q3DSLayer3D::engine() const
{
    return m_engine;
}

void Q3DSLayer3D::setEngine(QObject *e)
{
    Q3DSStudio3DEngine *item = qobject_cast<Q3DSStudio3DEngine *>(e);
    if (e && !item) {
        qWarning() << e << "is not a Studio3DEngine";
        return;
    }

    if (m_engine == item)
        return;

    if (m_engine)
        m_engine->unregisterView(this);

    m_engine = item;
    m_engineChanged = true;

    if (m_engine) {
        m_engine->registerView(this);
        connect(m_engine, &QObject::destroyed, this, [this](QObject *obj) {
            if (obj == m_engine) {
                m_engine = nullptr;
                m_presentation = nullptr;
                m_layer3DS = nullptr;
            }
        });
    }

    sync();

    emit engineChanged();
    if (window())
        window()->update();
}

static QByteArray makeIdAndName(Q3DSLayer3D *obj)
{
    QByteArray id = obj->metaObject()->className();
    id += QByteArrayLiteral("_0x");
    id += QByteArray::number((quintptr) obj, 16);
    return id;
}

void Q3DSLayer3D::sync()
{
    if (!m_layer3DS) {
        m_engineChanged = false;

        if (!m_engine)
            return;

        m_presentation = m_engine->presentation();
        if (!m_presentation)
            return;

        // id is always unique, name does not have to be. We will use the same unique string for both.
        const QByteArray id = makeIdAndName(this);
        m_layer3DS = m_presentation->newObject<Q3DSLayerNode>(id);
        m_layer3DS->setName(QString::fromLatin1(id));

        // Communicate the explicit size of the layer as early as possible in
        // order to avoid creating textures with a dummy initial size.
        if (window()) {
            const QSize sz = size().toSize() * window()->effectiveDevicePixelRatio();
            m_engine->handleViewGeometryChange(this, sz);
        }

        // Notify the waiting Object3D, if there is one, to create its subtree
        // and parent it to our m_layer3DS.
        emit layerNodeCreated();

        slide()->addObject(m_layer3DS);
        // The children of m_layer3DS are assumed to be already added to the
        // slide, that is the responsibility of the layerNodeCreated handler in
        // Q3DSObject3D on this path.

        // The order of Layer nodes in the Q3DSGraphObject tree does not matter,
        // it does not have to match the Layer3D order in the QQuickItem tree.
        // The moment of truth. This is when things become alive.
        m_presentation->scene()->appendChildNode(m_layer3DS);

    } else if (m_engineChanged) {
        m_engineChanged = false;

        if (m_presentation) {
            Q3DSUipPresentation::forAllObjectsInSubTree(m_layer3DS, [this](Q3DSGraphObject *obj) {
                slide()->removeObject(obj);
            });
            m_presentation->scene()->removeChildNode(m_layer3DS);
        }

        if (!m_engine) {
            m_presentation = nullptr;
            // engine was set to null, keep the layer object around since
            // we may get associated with the same or another engine later on
            return;
        }

        m_presentation = m_engine->presentation();
        if (m_presentation) {
            Q3DSUipPresentation::forAllObjectsInSubTree(m_layer3DS, [this](Q3DSGraphObject *obj) {
                slide()->addObject(obj);
            });
            m_presentation->scene()->appendChildNode(m_layer3DS);
        }
    }

    if (window() && m_engine) {
        const QSize sz = size().toSize() * window()->effectiveDevicePixelRatio();
        m_engine->handleViewGeometryChange(this, sz);
    }
}

bool Q3DSLayer3D::isTextureProvider() const
{
    return QQuickItem::isTextureProvider() || m_canProvideTexture;
}

QSGTextureProvider *Q3DSLayer3D::textureProvider() const
{
    // render thread

    if (QQuickItem::isTextureProvider())
        return QQuickItem::textureProvider();

    Q_ASSERT(m_canProvideTexture);

    if (!m_textureProvider) {
        m_textureProvider = new Q3DSLayerTextureProvider;
        m_textureProvider->setTexture(m_lastTexture);
    }

    return m_textureProvider;
}

void Q3DSLayer3D::notifyTextureChange(QSGTexture *t, int msaaSampleCount)
{
    // render thread

    if (msaaSampleCount > 1) {
        // the scenegraph itself has no support for multisample textures
        m_canProvideTexture = false;
        return;
    }

    if (m_textureProvider)
        m_textureProvider->setTexture(t);

    m_lastTexture = t;
    m_canProvideTexture = true;
}

void Q3DSLayer3D::keyPressEvent(QKeyEvent *event)
{
    if (m_engine && m_engine->engine())
        m_engine->engine()->handleKeyPressEvent(event);
}

void Q3DSLayer3D::keyReleaseEvent(QKeyEvent *event)
{
    if (m_engine && m_engine->engine())
        m_engine->engine()->handleKeyReleaseEvent(event);
}

void Q3DSLayer3D::mousePressEvent(QMouseEvent *event)
{
    if (m_engine && m_engine->engine() && m_layer3DS)
        m_engine->engine()->handleMousePressEvent(event, m_layer3DS);
}

void Q3DSLayer3D::mouseMoveEvent(QMouseEvent *event)
{
    if (m_engine && m_engine->engine() && m_layer3DS)
        m_engine->engine()->handleMouseMoveEvent(event, m_layer3DS);
}

void Q3DSLayer3D::mouseReleaseEvent(QMouseEvent *event)
{
    if (m_engine && m_engine->engine() && m_layer3DS)
        m_engine->engine()->handleMouseReleaseEvent(event, m_layer3DS);
}

void Q3DSLayer3D::mouseDoubleClickEvent(QMouseEvent *event)
{
    if (m_engine && m_engine->engine() && m_layer3DS)
        m_engine->engine()->handleMouseDoubleClickEvent(event, m_layer3DS);
}

#if QT_CONFIG(wheelevent)
void Q3DSLayer3D::wheelEvent(QWheelEvent *event)
{
    if (m_engine && m_engine->engine() && m_layer3DS)
        m_engine->engine()->handleWheelEvent(event, m_layer3DS);
}
#endif

void Q3DSLayer3D::touchEvent(QTouchEvent *event)
{
    if (m_engine && m_engine->engine() && m_layer3DS)
        m_engine->engine()->handleTouchEvent(event, m_layer3DS);
}

QT_END_NAMESPACE
