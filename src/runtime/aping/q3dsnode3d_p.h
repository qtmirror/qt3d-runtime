/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef Q3DSNODE3D_P_H
#define Q3DSNODE3D_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "q3dsobject3d_p.h"

QT_BEGIN_NAMESPACE

class Q3DSV_PRIVATE_EXPORT Q3DSNode3D : public Q3DSObject3D
{
    Q_OBJECT
    Q_PROPERTY(QVector3D rotation READ rotation WRITE setRotation NOTIFY rotationChanged)
    Q_PROPERTY(QVector3D position READ position WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(QVector3D scale READ scale WRITE setScale NOTIFY scaleChanged)
    Q_PROPERTY(QVector3D pivot READ pivot WRITE setPivot NOTIFY pivotChanged)

public:
    Q3DSNode3D();
    ~Q3DSNode3D();

    Q3DSNode *node() const { return static_cast<Q3DSNode *>(object()); }

    QVector3D scale() const { return m_scale; }
    QVector3D rotation() const { return m_rotation; }
    QVector3D position() const { return m_position; }
    QVector3D pivot() const { return m_pivot; }

public Q_SLOTS:
    void setScale(const QVector3D &scale);
    void setRotation(const QVector3D &rotation);
    void setPosition(const QVector3D &position);
    void setPivot(const QVector3D &pivot);

Q_SIGNALS:
    void scaleChanged(const QVector3D &scale);
    void rotationChanged(const QVector3D &rotation);
    void positionChanged(const QVector3D &position);
    void pivotChanged(const QVector3D &pivot);

private:
    QVector3D m_scale {1, 1, 1};
    QVector3D m_rotation;
    QVector3D m_position;
    QVector3D m_pivot;
};

QT_END_NAMESPACE

#endif // Q3DSNODE3D_P_H
