/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef Q3DSLAYER3D_P_H
#define Q3DSLAYER3D_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <Qt3DStudioRuntime2/private/q3dsruntimeglobal_p.h>
#include <QQuickItem>

QT_BEGIN_NAMESPACE

class Q3DSStudio3DEngine;
class Q3DSLayerTextureProvider;
class Q3DSLayerNode;
class Q3DSUipPresentation;
class Q3DSSlide;
class QSGTexture;
class Q3DSLayer3DSGNode;

class Q3DSV_PRIVATE_EXPORT Q3DSLayer3D : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QObject *engine READ engine WRITE setEngine NOTIFY engineChanged)

public:
    explicit Q3DSLayer3D(QQuickItem *parent = 0);
    ~Q3DSLayer3D();

    QObject *engine() const;
    void setEngine(QObject *e);

    bool isTextureProvider() const override;
    QSGTextureProvider *textureProvider() const override;

    Q3DSLayer3DSGNode *node() const { return m_node; }
    void notifyTextureChange(QSGTexture *t, int msaaSampleCount);

    Q3DSUipPresentation *presentation() const { return m_presentation; }
    Q3DSSlide *slide() const;
    Q3DSLayerNode *layerNode() const { return m_layer3DS; }

    bool isLive() const { return layerNode() && presentation(); }

signals:
    void engineChanged();
    void layerNodeCreated();

private:
    QSGNode *updatePaintNode(QSGNode *node, UpdatePaintNodeData *nodeData) override;
    void itemChange(QQuickItem::ItemChange change, const QQuickItem::ItemChangeData &changeData) override;
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry) override;
    void componentComplete() override;

    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;
#if QT_CONFIG(wheelevent)
    void wheelEvent(QWheelEvent *event) override;
#endif
    void touchEvent(QTouchEvent *event) override;

    void sync();

    Q3DSStudio3DEngine *m_engine = nullptr;
    bool m_engineChanged = false;
    Q3DSUipPresentation *m_presentation = nullptr;
    mutable Q3DSLayerTextureProvider *m_textureProvider = nullptr;
    bool m_canProvideTexture = true;
    QSGTexture *m_lastTexture = nullptr;
    Q3DSLayerNode *m_layer3DS = nullptr;
    QSGTexture *m_dummyTexture = nullptr;
    Q3DSLayer3DSGNode *m_node = nullptr;
};

QT_END_NAMESPACE

#endif // Q3DSLAYER3D_P_H
