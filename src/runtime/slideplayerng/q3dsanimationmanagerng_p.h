/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef Q3DSANIMATIONMANAGER_NG_P_H
#define Q3DSANIMATIONMANAGER_NG_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "q3dsuippresentation_p.h"
#include "animator/q3dsanimator_p.h"

QT_BEGIN_NAMESPACE

namespace Q3DSAnimationManagerNg
{
    Q3DSV_PRIVATE_EXPORT void setActive(Q3DSSlide *slide, bool active);
    Q3DSV_PRIVATE_EXPORT void advance(Q3DSSlide *slide, float dtMs);
    Q3DSV_PRIVATE_EXPORT void goToTime(Q3DSSlide *slide, float timeMs, bool sync = false);
    Q3DSV_PRIVATE_EXPORT void setRate(Q3DSSlide *slide, float rate);
    Q3DSV_PRIVATE_EXPORT void setDuration(Q3DSSlide *slide, float durationMs);
    Q3DSV_PRIVATE_EXPORT void setPlayMode(Q3DSSlide *slide, Q3DSSlide::PlayMode mode);
    Q3DSV_PRIVATE_EXPORT void setEosCallback(Q3DSSlide *slide, Q3DSAnimator::EosCallback cb);
    Q3DSV_PRIVATE_EXPORT void setTimeChangeCallback(Q3DSSlide *slide, Q3DSAnimator::TimeChangeCallback cb);
    Q3DSV_PRIVATE_EXPORT void buildSlideAnimation(Q3DSSlide *slide, bool rebuild = false);
    Q3DSV_PRIVATE_EXPORT void updateDynamicKeyFrames(Q3DSSlide *slide);
    Q3DSV_PRIVATE_EXPORT void addObject(Q3DSSlide *slide, Q3DSGraphObject *obj);
    Q3DSV_PRIVATE_EXPORT void removeObject(Q3DSSlide *slide, Q3DSGraphObject *obj);
    Q3DSV_PRIVATE_EXPORT void destroyAnimator(Q3DSSlide *slide);
    Q3DSV_PRIVATE_EXPORT void setTrackEnabled(Q3DSSlide *slide, const Q3DSAnimationTrack &track, bool enabled);
};

QT_END_NAMESPACE

#endif // Q3DSANIMATIONMANAGER_NG_P_H
