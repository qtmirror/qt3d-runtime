/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "q3dsslideplayerng_p.h"

#include <QtCore/qglobal.h>
#include <QtCore/qloggingcategory.h>
#include <QtCore/qmetaobject.h>
#include "q3dsscenemanager_p.h"
#include "q3dsanimationmanagerng_p.h"
#include "q3dsengine_p.h"
#include "q3dslogging_p.h"
#include "animator/q3dsanimator_p.h"

QT_BEGIN_NAMESPACE

template <typename Enum>
const char *getEnumName(Enum e)
{
    return QMetaEnum::fromType<Enum>().valueToKey(int(e));
}

// NOTE: This will skip components that are marked as disabled!!!
static void forAllSlideComponents(Q3DSSlide *slide, std::function<void (Q3DSComponentNode *)> fn)
{
    if (slide) {
        const auto &objects = slide->objects();
        auto it = objects.cbegin();
        while (it != objects.cend()) {
            if ((*it)->type() == Q3DSGraphObject::Component && (*it)->state() == Q3DSGraphObject::Enabled)
                fn(static_cast<Q3DSComponentNode *>(*it));
            ++it;
        }
    }
}

bool Q3DSSlideUtils::useDragonWings() {
    static bool b = (qEnvironmentVariableIntValue("Q3DS_NO_DRAGONWINGS") > 0);
    static bool first = true;
    if (b && first) {
        first = false;
        qCDebug(lcAnim, "DragonWings animator disabled!");
    }
    return !b;
}

void Q3DSSlideUtils::getStartAndEndTime(Q3DSSlide *slide, qint32 *startTime, qint32 *endTime)
{
    Q_ASSERT(startTime != nullptr || endTime != nullptr);

    qint32 layerEndTime = -1;
    qint32 nodesEndtime = -1;
    bool layerFound = false; // If there's no layer object, then we can assume this is a component.

    // TODO: If we know the slide is "current" then the properties should be applied already and
    // we can skip this step.

    // Check if there are nodes from the parent slide that has property changes on this slide.
    if (Q3DSSlide *p = static_cast<Q3DSSlide *>(slide->parent())) {
        for (auto *obj : p->objects()) {
            if (!obj->isNode() || obj->state() != Q3DSGraphObject::Enabled)
                continue;

            // If we find a layer object we can't be a component!
            if (!layerFound && obj->type() == Q3DSGraphObject::Layer)
                layerFound = true;

            // Look for property updates on "this" slide.
            const auto &props = slide->propertyChanges();
            const auto foundIt = props.constFind(obj);
            if (foundIt == props.constEnd())
                continue;

            // If there are property changes for the object, check if it has a new endtime.
            std::find_if(foundIt.value()->cbegin(), foundIt.value()->cend(), [&layerEndTime, &nodesEndtime, obj](const Q3DSPropertyChange &propChange) {
                if (propChange.name() == QLatin1String("endtime")) {
                    bool ok = false;
                    const qint32 value = propChange.value().toInt(&ok);
                    if (ok) {
                        if (obj->type() == Q3DSGraphObject::Layer && (value > layerEndTime))
                            layerEndTime = value;
                        else if (value > nodesEndtime)
                            nodesEndtime = value;
                    }
                }
                return false;
            });
        }
    }

    // So... There was a layer (on the master slide), but there was no property change
    // for it on this slide, then it has the default time of 10s.
    if (layerFound && layerEndTime == -1)
        layerEndTime = slide->endTime(); // 10s

    // Now look for the endtime on nodes on this slide.
    for (const auto obj : slide->objects()) {
        // Skip non-node types.
        if (!obj->isNode() || obj->state() != Q3DSGraphObject::Enabled)
            continue;

        // We collect both layer endtimes (if any) and object endtimes in one go.
        if (obj->type() == Q3DSGraphObject::Layer && (obj->endTime() > layerEndTime))
            layerEndTime = obj->endTime();
        else if (obj->endTime() > nodesEndtime)
            nodesEndtime = obj->endTime();
    }

    // In a scene there's at least one layer (the master slide's layer)
    // The master slide layer's duration might be changed by any of the child slides (see step above).
    // If there's more then one layer, then the start time will be the layer with the startTime closest
    // to 0, while the endTime will be the layer with the greatest endTime.
    //
    // Exception!!! If the slide is on a component then there's no layer and we need to
    // look at the nodes start and end times.

    // Final fallback, if neither was found use the value set by the slide.
    if (layerEndTime == -1 && nodesEndtime == -1)
        nodesEndtime = slide->endTime();

    if (startTime)
        *startTime = slide->startTime();
    if (endTime)
        *endTime = layerEndTime != -1 ? layerEndTime : nodesEndtime;
}

static QByteArray getSlideId(Q3DSSlide *slide)
{
    return slide ? slide->id() : QByteArrayLiteral("no-slide");
}

static Q3DSSlidePlayerNg::PlayerState getInitialSlideState(Q3DSSlide *slide)
{
    return (slide->initialPlayState() == Q3DSSlide::Play) ? Q3DSSlidePlayerNg::PlayerState::Playing
                                                          : Q3DSSlidePlayerNg::PlayerState::Paused;

}

Q3DSSlidePlayerNg::Q3DSSlidePlayerNg(Q3DSSceneManager *sceneManager,
                                     Q3DSAbstractSlidePlayer *parent)
    : Q3DSAbstractSlidePlayer(parent)
    , m_sceneManager(sceneManager)
{
}

Q3DSSlidePlayerNg::~Q3DSSlidePlayerNg()
{
    reset();
}

Q3DSSlideDeck *Q3DSSlidePlayerNg::slideDeck() const
{
    return m_data.slideDeck;
}

void Q3DSSlidePlayerNg::advanceFrame(float dt)
{
    if (m_data.state != PlayerState::Playing && m_data.state != PlayerState::Paused)
        return;

    // NOTE: dt is in ms!
    Q3DSSlide *slide = m_data.slideDeck->currentSlide();
    if (slide->parent())
        Q3DSAnimationManagerNg::advance(static_cast<Q3DSSlide *>(slide->parent()), dt);
    Q3DSAnimationManagerNg::advance(slide, dt);
    forAllSlideComponents(static_cast<Q3DSSlide *>(slide->parent()), [dt](Q3DSComponentNode *component) {
        component->currentSlide()->attached<Q3DSSlideAttached>()->slidePlayer->advanceFrame(dt);
    });
    forAllSlideComponents(slide, [dt](Q3DSComponentNode *component) {
        component->currentSlide()->attached<Q3DSSlideAttached>()->slidePlayer->advanceFrame(dt);
    });
}

void Q3DSSlidePlayerNg::sceneReady()
{
    Q3DSSlideDeck *slideDeck = m_data.slideDeck;
    if (!slideDeck)
        return;

    Q3DSSlide *currentSlide = slideDeck->currentSlide();
    Q_ASSERT(currentSlide);

    const bool viewerMode = (m_mode == PlayerMode::Viewer);
    if (viewerMode && (currentSlide->initialPlayState() == Q3DSSlide::Play))
        setInternalState(PlayerState::Playing);
    else
        setInternalState(PlayerState::Paused);
}

float Q3DSSlidePlayerNg::duration() const
{
    if (m_data.state == PlayerState::Idle)
        return 0;

    return m_data.duration;
}

float Q3DSSlidePlayerNg::position() const
{
    return m_data.position;
}

void Q3DSSlidePlayerNg::setMode(Q3DSSlidePlayerNg::PlayerMode mode)
{
    m_mode = mode;
    if (!m_data.slideDeck)
        return;

    const auto setComponentMode = [mode](Q3DSComponentNode *comp) {
        comp->masterSlide()->attached<Q3DSSlideAttached>()->slidePlayer->setMode(mode);
    };

    Q3DSSlide *slide = m_data.slideDeck->masterSlide();
    forAllSlideComponents(slide, setComponentMode);
    slide = static_cast<Q3DSSlide *>(slide->firstChild());
    while (slide) {
        forAllSlideComponents(slide, setComponentMode);
        slide = static_cast<Q3DSSlide *>(slide->nextSibling());
    }

    reload();
}

void Q3DSSlidePlayerNg::play()
{
    if (m_data.state == PlayerState::Playing)
        return;

    if (m_data.state == PlayerState::Idle) {
        qCWarning(lcSlidePlayer) << "Play called in Idle state (no content)";
        return;
    }

    Q3DSSlideDeck *slideDeck = m_data.slideDeck;
    Q_ASSERT(slideDeck);
    Q_ASSERT(!slideDeck->isEmpty());

    Q3DSSlide *currentSlide = slideDeck->currentSlide();
    if (!currentSlide) {
        qCWarning(lcSlidePlayer, "No slide selected!");
        return;
    }

    setInternalState(PlayerState::Playing);
}

void Q3DSSlidePlayerNg::stop()
{
    if (m_data.state == PlayerState::Idle) {
        qCWarning(lcSlidePlayer) << "Stop called in Idle state (no content)";
        return;
    }

    Q3DSSlideDeck *slideDeck = m_data.slideDeck;
    Q_ASSERT(slideDeck);
    Q_ASSERT(!slideDeck->isEmpty());

    Q3DSSlide *currentSlide = slideDeck->currentSlide();
    if (!currentSlide) {
        qCWarning(lcSlidePlayer, "No slide selected!");
        return;
    }

    setInternalState(PlayerState::Stopped);
}

void Q3DSSlidePlayerNg::pause()
{
    if (m_data.state == PlayerState::Paused)
        return;

    if (m_data.state == PlayerState::Idle) {
        qCWarning(lcSlidePlayer) << "Pause called in Idle state (no content)";
        return;
    }

    setInternalState(PlayerState::Paused);
}

void Q3DSSlidePlayerNg::seek(float pos)
{
    if (qFuzzyCompare(m_data.position, pos))
        return;

    Q3DSSlideDeck *slideDeck = m_data.slideDeck;
    if (!slideDeck)
        return;

    if (slideDeck->masterSlide())
        Q3DSAnimationManagerNg::goToTime(slideDeck->masterSlide(), pos);
    if (slideDeck->currentSlide())
        Q3DSAnimationManagerNg::goToTime(slideDeck->currentSlide(), pos);
}

void Q3DSSlidePlayerNg::setSlideDeck(Q3DSSlideDeck *slideDeck)
{
    if (m_data.slideDeck == slideDeck) {
        qCWarning(lcSlidePlayer, "Setting same slide deck (nop)");
        return;
    }

    const bool forceReset = (m_data.state != PlayerState::Idle || slideDeck == nullptr);
    if (forceReset)
        reset();

    if (slideDeck == nullptr || slideDeck->isEmpty()) {
        qCWarning(lcSlidePlayer, "Got an empty slide deck!");
        return;
    }

    m_data.slideDeck = slideDeck;
    m_data.slideDeck->bind(this);

    qCDebug(lcSlidePlayer, "Setting slide deck with %d slides", slideDeck->slideCount());

    // This will recurse down all the slides and components slides
    const auto prepareComponentsOnSlide = [this](Q3DSSlide *slide, Q3DSGraphObject *obj) {
        // Check if the owning slide already has a player for this component
        Q3DSComponentNode *comp = static_cast<Q3DSComponentNode *>(obj);

        Q3DSSlide *compMasterSlide = comp->masterSlide();
        Q3DSSlideAttached *compMasterData = compMasterSlide->attached<Q3DSSlideAttached>();
        Q_ASSERT(compMasterData);
        qCDebug(lcSlidePlayer, "Processing component \"%s\", on slide \"%s\"",
                comp->id().constData(), getSlideId(slide).constData());
        if (!compMasterData->slidePlayer) {
            qCDebug(lcSlidePlayer, "No player found for Component \"%s\", adding one", qPrintable(comp->name()));
            compMasterData->slidePlayer = new Q3DSSlidePlayerNg(m_sceneManager, comp, this);
            Q3DSSlide *s = static_cast<Q3DSSlide *>(compMasterSlide->firstChild());
            while (s) {
                if (!s->attached())
                    s->setAttached(new Q3DSSlideAttached);
                s = static_cast<Q3DSSlide *>(s->nextSibling());
            }

            // Create a slide deck for this component
            compMasterData->slidePlayer->setSlideDeck(new Q3DSSlideDeck(compMasterSlide, slide));
        }
    };

    const auto forAllComponentsOnSlide = [prepareComponentsOnSlide](Q3DSSlide *slide) {
        Q3DSSlideAttached *data = slide->attached<Q3DSSlideAttached>();
        if (!data) {
            data = new Q3DSSlideAttached;
            slide->setAttached(data);
        }

        for (auto object : slide->objects()) {
            if (object->type() == Q3DSGraphObject::Component && object->state() == Q3DSGraphObject::Enabled)
                prepareComponentsOnSlide(slide, object);
        }
    };

    const auto forAllSlides = [&forAllComponentsOnSlide, this](Q3DSSlideDeck *slideDeck) {
        // Process the master slide first
        Q3DSSlide *masterSlide = slideDeck->masterSlide();
        masterSlide->attached<Q3DSSlideAttached>()->slidePlayer = this;
        forAllComponentsOnSlide(masterSlide);
        Q3DSSlide *slide = static_cast<Q3DSSlide *>(masterSlide->firstChild());
        while (slide) {
            slide->attached<Q3DSSlideAttached>()->slidePlayer = this;
            forAllComponentsOnSlide(slide);
            slide = static_cast<Q3DSSlide *>(slide->nextSibling());
        }
    };

    forAllSlides(slideDeck);
    m_sceneManager->engine()->loadSlideResources(slideDeck->masterSlide(),
                                                 m_sceneManager->presentation());
    for (auto object : slideDeck->masterSlide()->objects()) {
        if (object->type() == Q3DSGraphObject::Component
                && object->state() == Q3DSGraphObject::Enabled) {
            Q3DSComponentNode *comp = static_cast<Q3DSComponentNode *>(object);
            m_sceneManager->engine()->loadSlideResources(comp->masterSlide(),
                                                         m_sceneManager->presentation());
            m_sceneManager->engine()->loadSlideResources(comp->currentSlide(),
                                                         m_sceneManager->presentation());
        }
    }
    setInternalState(PlayerState::Ready);
    Q_EMIT slideDeckChanged(m_data.slideDeck);
}

void Q3DSSlidePlayerNg::setPlaybackRate(float rate)
{
    Q3DSSlideDeck *slideDeck = m_data.slideDeck;
    if (!slideDeck)
        return;

    Q3DSSlide *slide = slideDeck->currentSlide();
    if (!slide)
        return;

    Q3DSAnimationManagerNg::setRate(slide, rate);
    m_data.playbackRate = rate;
}

void Q3DSSlidePlayerNg::nextSlide()
{
    if (m_data.state == PlayerState::Idle)
        return;

    m_data.slideDeck->goToNextSlide();
    reload();
}

void Q3DSSlidePlayerNg::previousSlide()
{
    if (m_data.state == PlayerState::Idle)
        return;

    m_data.slideDeck->gotToPreviousSlide();
    reload();
}

void Q3DSSlidePlayerNg::precedingSlide()
{
    if (m_data.state == PlayerState::Idle)
        return;

    m_data.slideDeck->precedingSlide();
    reload();
}

void Q3DSSlidePlayerNg::reload()
{
    Q3DSSlide *slide = m_data.slideDeck->currentSlide();
    // We need to make sure the slide is visible
    const bool slideVisible = isSlideVisible(slide);
    setInternalState(slideVisible ? ((m_mode == PlayerMode::Viewer) ? getInitialSlideState(slide) : PlayerState::Paused) : PlayerState::Ready);
}

Q3DSSlidePlayerNg::Q3DSSlidePlayerNg(Q3DSSceneManager *sceneManager,
                                     Q3DSComponentNode *component,
                                     Q3DSAbstractSlidePlayer *parent)
    : Q3DSAbstractSlidePlayer(parent),
      m_sceneManager(sceneManager),
      m_component(component),
      m_mode(parent->mode()),
      m_type(PlayerType::Component)
{
}

void Q3DSSlidePlayerNg::init()
{

}

void Q3DSSlidePlayerNg::reset()
{
    setInternalState(PlayerState::Idle);
}

void Q3DSSlidePlayerNg::changeState(Q3DSSlide *slide, Q3DSSlidePlayerNg::PlayerState state)
{
    // The scene state is the slide and the player state is the state of the animation
    if (!slide)
        return;

    qCDebug(lcSlidePlayer, "Changing state for %s to %s", getSlideId(slide).constData(), getEnumName(state));

    // 1. Only one slide is active at the time (and one for each sub-component).
    static const auto updateComponentState = [](Q3DSComponentNode *comp) {
        Q3DSSlide *compSlide = comp->currentSlide();
        Q3DSSlideAttached *data = compSlide->attached<Q3DSSlideAttached>();
        static_cast<Q3DSSlidePlayerNg *>(data->slidePlayer)->reload();
    };

    static const auto resetComponentState = [](Q3DSComponentNode *comp) {
        Q3DSSlide *compSlide = comp->currentSlide();
        Q3DSSlideAttached *data = compSlide->attached<Q3DSSlideAttached>();
        static_cast<Q3DSSlidePlayerNg *>(data->slidePlayer)->setInternalState(PlayerState::Ready);
    };

    switch (state) {
    case PlayerState::Idle:
        Q3DSAnimationManagerNg::destroyAnimator(slide);
        if (m_data.slideDeck)
            delete m_data.slideDeck;
        m_data = Data();
        break;
    case PlayerState::Paused:
        Q3DSAnimationManagerNg::setActive(slide, false);
        forAllSlideComponents(slide, updateComponentState);
        Q3DSAnimationManagerNg::goToTime(slide, m_data.position, true);
        break;
    case PlayerState::Playing:
        Q3DSAnimationManagerNg::goToTime(slide, m_data.position, true);
        forAllSlideComponents(slide, updateComponentState);
        Q3DSAnimationManagerNg::setActive(slide, true);
        break;
    case PlayerState::Ready: // Reset to the initial state
        Q3DSAnimationManagerNg::goToTime(slide, m_data.position, true);
        Q3DSAnimationManagerNg::setActive(slide, false);
        // Hide all objects (NB: No point in hiding the master slide's objects)
        if (slide->parent())
            setObjectVisibility(slide, -1.0f);
        forAllSlideComponents(slide, resetComponentState);
        break;
    case PlayerState::Stopped:
        // Make the slides animations inactive and reset the local time to 0
        Q3DSAnimationManagerNg::setActive(slide, false);
        Q3DSAnimationManagerNg::goToTime(slide, 0.0f, true);
        break;
    }
}

void Q3DSSlidePlayerNg::setInternalState(Q3DSSlidePlayerNg::PlayerState newState)
{
    const auto queueSlideEvent = [this](Q3DSSlide *slide, const QString &evName) {
        if (!slide)
            return;

        Q3DSGraphObject *eventTarget = m_sceneManager->m_scene;
        if (m_type == PlayerType::Component)
            eventTarget = m_component;

        const QVariantList args { QVariant::fromValue(slide), m_data.slideDeck->indexOfSlide(slide->id()) };
        const Q3DSGraphObject::Event ev(eventTarget, evName, { QVariant::fromValue(slide), m_data.slideDeck->indexOfSlide(slide->id())});
        m_sceneManager->queueEvent(ev);
    };

    Q3DSSlide *oldSlide = m_currentSlide;
    Q3DSSlide *currentSlide = m_data.slideDeck->currentSlide();
    Q_ASSERT(currentSlide);
    m_sceneManager->engine()->loadSlideResources(currentSlide, m_sceneManager->presentation());
    if (m_currentSlide && m_currentSlide != currentSlide) {
        m_currentSlide->setActive(false);
        if (m_currentSlide->unloadSlide())
            m_sceneManager->engine()->unloadSlideResources(m_currentSlide,
                                                           m_sceneManager->presentation());
    }
    m_currentSlide = currentSlide;
    m_currentSlide->setActive(true);
    for (auto object : currentSlide->objects()) {
        if (object->type() == Q3DSGraphObject::Component
                && object->state() == Q3DSGraphObject::Enabled) {
            Q3DSComponentNode *comp = static_cast<Q3DSComponentNode *>(object);
            m_sceneManager->engine()->loadSlideResources(comp->masterSlide(),
                                                         m_sceneManager->presentation());
            m_sceneManager->engine()->loadSlideResources(comp->currentSlide(),
                                                         m_sceneManager->presentation());
        }
    }

    Q3DSSlide *activeSlide = (m_type == PlayerType::Scene) ? m_sceneManager->currentSlide()
                                                           : m_component->currentSlide();

    // If there's no previous slide in the slide deck, then this is an actual slide change event
    if (m_data.state == PlayerState::Ready && !m_data.slideDeck->previousSlide())
        activeSlide = nullptr;

    const bool parentChanged = activeSlide ? (activeSlide->parent() != currentSlide->parent()) : true;

    // Slide changed or the current slide has not gotten its animator built yet.
    if (activeSlide != currentSlide) {
        // If the slide changed, move the previous slide into ready state first.
        if (activeSlide && m_data.state != PlayerState::Ready) {
            if (parentChanged)
                changeState(static_cast<Q3DSSlide *>(activeSlide->parent()), PlayerState::Ready);
            changeState(activeSlide, PlayerState::Ready);
        }

        // TODO:
        // 1. Build all animation once (might be option to release them, but we'll get back to that).
        // 2. Make it possible to do lazy loading, i.e., build the animatio on first use
        // 3. Animators are either active or inactive, nothing more!

        // If we have a new slide we need to update the property changes for that slide.
        // This also makes sure we don't overwrite the values for properties with dynamic key frames
        if ((newState != PlayerState::Idle) && currentSlide && isSlideVisible(currentSlide)) {
            // Update the scene/component to hold the current slide
            if (m_type == PlayerType::Scene)
                m_sceneManager->m_currentSlide = currentSlide;
            else
                m_component->setCurrentSlide(currentSlide);

            // Ensure that the slide's animation tracks are built
            Q3DSAnimationManagerNg::buildSlideAnimation(static_cast<Q3DSSlide *>(currentSlide->parent()));
            Q3DSAnimationManagerNg::buildSlideAnimation(currentSlide);
            // If this is a real slide change (previous slide != null and != current), then
            // we need to update the dynamic key frames
            if (activeSlide && activeSlide != currentSlide)
                Q3DSAnimationManagerNg::updateDynamicKeyFrames(currentSlide);
            // All set, now apply the property changes for this slide. Note that the values might
            // be out of sync after this, but once the slide changes to playing/paused, the values
            // will be synced to match the values from the animator (based on the first kf values).
            processPropertyChanges(static_cast<Q3DSSlide *>(currentSlide->parent()));
            processPropertyChanges(currentSlide);
            Q_ASSERT(currentSlide->attached<Q3DSSlideAttached>()->animatorNg);


            qint32 endTime = 0; // ms
            Q3DSSlideUtils::getStartAndEndTime(currentSlide, nullptr, &endTime);
            onDurationChanged(endTime);
            m_data.position = 0.0f;

            // Update duration, rate, and playmode
            // If the current slide has a parent, then the parent inherits its child's mode, duration, etc.
            Q3DSAnimationManagerNg::setPlayMode(static_cast<Q3DSSlide *>(currentSlide->parent()), currentSlide->playMode());
            Q3DSAnimationManagerNg::setDuration(static_cast<Q3DSSlide *>(currentSlide->parent()), m_data.duration);
            Q3DSAnimationManagerNg::setRate(static_cast<Q3DSSlide *>(currentSlide->parent()), m_data.playbackRate);

            Q3DSAnimationManagerNg::setPlayMode(currentSlide, currentSlide->playMode());
            Q3DSAnimationManagerNg::setDuration(currentSlide, m_data.duration);
            Q3DSAnimationManagerNg::setRate(currentSlide, m_data.playbackRate);

            // Attach callbacks
            Q3DSAnimationManagerNg::setEosCallback(currentSlide, [this, currentSlide]() { onSlideFinished(currentSlide); });
            Q3DSAnimationManagerNg::setTimeChangeCallback(currentSlide, [this, currentSlide](float time) {
                time = qRound(time * 1000.f); // We get time in seconds, so adjust
                setObjectVisibility(static_cast<Q3DSSlide *>(currentSlide->parent()), time);
                setObjectVisibility(currentSlide, time);
                sendPositionChanged(time);
            });

            changeState(static_cast<Q3DSSlide *>(currentSlide->parent()), newState);
            changeState(currentSlide, newState);
            m_sceneManager->syncScene();

            if (newState != PlayerState::Idle) {
                if (oldSlide)
                    queueSlideEvent(oldSlide, Q3DSGraphObjectEvents::slideExitEvent());
                queueSlideEvent(currentSlide, Q3DSGraphObjectEvents::slideEnterEvent());
                Q_EMIT slideChanged(currentSlide);
            }
        }
    }

    if (m_data.state != newState) {
        // The slide didn't change but the state did, so call changeState() now!
        if (currentSlide == activeSlide) {
            changeState(static_cast<Q3DSSlide *>(currentSlide->parent()), newState);
            changeState(currentSlide, newState);
        }
        m_data.state = newState;
        Q_EMIT stateChanged(m_data.state);
    }
}

static bool objectHasVisibilityTag(Q3DSGraphObject *object)
{
    Q_ASSERT(object);
    Q_ASSERT(object->attached());
    Q_ASSERT(object->isNode() || object->type() == Q3DSGraphObject::Effect);

    return (object->attached()->visibilityTag == Q3DSGraphObjectAttached::Visible);
}

void Q3DSSlidePlayerNg::setObjectVisibility(Q3DSSlide *slide, float time, bool forceUpdate)
{
    if (!slide)
        return;

    bool parentVisible = true;
    // If this is a component player, then check if the component is visible.
    if (m_type == Q3DSSlidePlayerNg::PlayerType::Component)
        parentVisible = objectHasVisibilityTag(m_component);

    for (Q3DSGraphObject *obj : slide->objects())
        setObjectVisibility(obj, parentVisible, forceUpdate, time);

    m_sceneManager->setPendingVisibilities();
}

void Q3DSSlidePlayerNg::setObjectVisibility(Q3DSGraphObject *obj, bool parentVisible, bool forceUpdate, float time)
{
    if (obj->state() != Q3DSGraphObject::Enabled)
        return;

    const bool isEffect = (obj->type() == Q3DSGraphObject::Effect);
    if ((!obj->isNode() && !isEffect))
        return;

    bool nodeActive = false;
    if (obj->isNode()) {
        Q3DSNode *node = static_cast<Q3DSNode *>(obj);
        if (node && !node->attached())
            return;

        if (node) {
            nodeActive = node->flags().testFlag(Q3DSNode::Active);
            if (node->parent() && node->parent()->isNode())
                nodeActive &= node->parent()->attached<Q3DSNodeAttached>()->globalLogicalVisibility;
        }
    }

    const bool effectActive = (isEffect && static_cast<Q3DSEffectInstance *>(obj)->eyeballEnabled());
    const bool shouldBeVisible = parentVisible
            && time >= obj->startTime() && time <= obj->endTime()
            && (nodeActive || effectActive);

    if (forceUpdate || shouldBeVisible != objectHasVisibilityTag(obj))
        updateObjectVisibility(obj, shouldBeVisible, time);
}

void Q3DSSlidePlayerNg::sendPositionChanged(float pos)
{
    const float oldPosition = m_data.position;
    m_data.position = pos;
    if (!qFuzzyCompare(oldPosition, pos))
        Q_EMIT positionChanged(pos);
}

void Q3DSSlidePlayerNg::updateObjectVisibility(Q3DSGraphObject *obj, bool shouldBeVisible, float time)
{
    Q_ASSERT(obj->isNode() || obj->type() == Q3DSGraphObject::Effect);
    auto foundIt = m_sceneManager->m_pendingObjectVisibility.find(obj);
    const bool insertValue = (foundIt == m_sceneManager->m_pendingObjectVisibility.end());
    const bool updateValue = (!insertValue && foundIt.value() != shouldBeVisible);

    if (insertValue || updateValue) {
        qCDebug(lcSlidePlayer, "Scheduling object \"%s\" to be %s at %.1fs", obj->id().constData(), shouldBeVisible ? "shown" : "hidden", double((time < 0.0f) ? time : (time / 1000.f)));
        if (updateValue)
            *foundIt = shouldBeVisible;
        else if (insertValue)
            m_sceneManager->m_pendingObjectVisibility.insert(obj, shouldBeVisible);
    }
}

bool Q3DSSlidePlayerNg::isSlideVisible(Q3DSSlide *slide)
{
    qCDebug(lcSlidePlayer, "Checking visibility for \"%s\"", getSlideId(slide).constData());
    // If we the slide is not null, we assume it's visible until proven otherwise.
    bool visible = (slide != nullptr);
    if (slide) {
        Q3DSSlidePlayerNg *player = static_cast<Q3DSSlidePlayerNg *>(slide->attached<Q3DSSlideAttached>()->slidePlayer);
        Q3DSSlideDeck *slideDeck = player->slideDeck();
        const bool isMasterSlide = (slideDeck->masterSlide() == slide);
        const bool isCurrentSlide = (slideDeck->currentSlide() == slide);
        if (isCurrentSlide || isMasterSlide) {
            Q3DSSlide *parentSlide = slideDeck->parentSlide();
            if (parentSlide) {
                // We're a component and current, continue up the ladder...
                visible = isSlideVisible(parentSlide);
            } else {
                visible = true;
            }
        } else {
            visible = false;
        }
    }

    qCDebug(lcSlidePlayer, "The slides's (\"%s\") visibility is %d", getSlideId(slide).constData(), visible);

    return  visible;
}

void Q3DSSlidePlayerNg::processPropertyChanges(Q3DSSlide *currentSlide)
{
    Q_ASSERT(currentSlide->attached());
    // Reset eyeball values
    // 1. Eyeball set to true on master and false on slideX -> eyeball updated by prop change for slideX
    // 2. Eyeball set to false on master and false on slideX -> eyeball update by prop change for slideX
    // 3. Eyeball set to false on master and true on slideX -> eyeball was update by reset
    // 4. Eyeball set to true on master and true on slideX -> noop
    if (currentSlide->parent()) {
        Q3DSSlide *parent = static_cast<Q3DSSlide *>(currentSlide->parent());
        const auto &objects = parent->objects();
        std::find_if(objects.constBegin(), objects.constEnd(), [](Q3DSGraphObject *object){
            if (object->state() != Q3DSGraphObject::Enabled)
                return false;

            if (!object->isNode() && object->type() != Q3DSGraphObject::Effect)
                return false;

            bool resetEyeball = false;
            if (object->isNode() && !static_cast<Q3DSNode *>(object)->eyeballEnabled())
                resetEyeball = true;
            else if (object->type() == Q3DSGraphObject::Effect && !static_cast<Q3DSEffectInstance *>(object)->eyeballEnabled())
                resetEyeball = true;

            if (resetEyeball) {
                object->applyPropertyChanges(Q3DSPropertyChangeList{Q3DSPropertyChange::fromVariant(QString::fromLatin1("eyeball"), QVariant::fromValue(true))});
                object->notifyPropertyChanges(Q3DSPropertyChangeList{Q3DSPropertyChange::fromVariant(QString::fromLatin1("eyeball"), QVariant::fromValue(true))});
            }

            return false;
        });

        // Make sure all tracks on the master slide are enabled again
        const auto &tracks = parent->animations();
        for (const auto &track : tracks)
            Q3DSAnimationManagerNg::setTrackEnabled(parent, track, true);
    }

    // Set the property values
    const auto &propertyChanges = currentSlide->propertyChanges();
    for (auto it = propertyChanges.cbegin(); it != propertyChanges.cend(); ++it) {
        it.key()->applyPropertyChanges(*it.value());
        it.key()->notifyPropertyChanges(*it.value());

        // If an object has a property set on it (with or without animation tracks) and the same
        // object has an animation track for that property on the master slide, then the track on the
        // master slide should be disabled.
        if (currentSlide->parent()) {
            Q3DSSlide *parent = static_cast<Q3DSSlide *>(currentSlide->parent());
            const auto &pt = parent->animations(); // parent tracks
            const auto &pl = it.value();
            auto plIt = pl->cbegin();
            const auto end = pl->cend();
            const auto target = it.key();
            while (plIt != end) {
                const auto foundIt = std::find_if(pt.cbegin(), pt.cend(), [plIt, target](const Q3DSAnimationTrack &track) {
                    const auto prop = track.property().split('.');
                    return (track.target() == target && prop[0] == plIt->name());
                });

                if (foundIt != pt.cend())
                    Q3DSAnimationManagerNg::setTrackEnabled(parent, *foundIt, false);
                ++plIt;
            }
        }
    }
}

void Q3DSSlidePlayerNg::onDurationChanged(float duration)
{
    if (qFuzzyCompare(duration, m_data.duration))
        return;

    m_data.duration = duration;
    Q_EMIT durationChanged(duration);
}

void Q3DSSlidePlayerNg::onSlideFinished(Q3DSSlide *slide)
{
    Q_ASSERT(m_data.slideDeck);
    Q_ASSERT(slide == m_data.slideDeck->currentSlide());

    qCDebug(lcSlidePlayer, "Slide %s finished at time: %.1fs", getSlideId(slide).constData(), double(m_data.position / 1000.f));

    PlayerState state = PlayerState::Stopped;

    // We don't change slides automatically in Editor mode
    if (m_mode == PlayerMode::Editor) {
        setInternalState(state);
        return;
    }

    // Get the slide's play mode
    const auto playMode = slide->playMode();

    if (playMode == Q3DSSlide::PlayThroughTo) {
        if (slide->playThrough() == Q3DSSlide::Next) {
            m_data.slideDeck->goToNextSlide();
        } else if (slide->playThrough() == Q3DSSlide::Previous) {
            m_data.slideDeck->gotToPreviousSlide();
        } else if (slide->playThrough() == Q3DSSlide::Value) {
            const auto value = slide->playThroughValue();
            if (value.type() == QVariant::Int) { // Assume this is a fixed index value
                m_data.slideDeck->setCurrentIndex(value.toInt());
            } else if (value.type() == QVariant::String) { // Reference to a slide?
                const QString &slideId = value.toString().mid(1);
                const int index = m_data.slideDeck->indexOfSlide(slideId.toLocal8Bit());
                if (!m_data.slideDeck->setCurrentIndex(index))
                    qCWarning(lcSlidePlayer, "Unable to make slide \"%s\" current", qPrintable(slideId));
            }
        }
        // Since we're jumping to a new slide, make sure we take the initial play-state into account.
        state = getInitialSlideState(m_data.slideDeck->currentSlide());
    } else if (playMode == Q3DSSlide::StopAtEnd || playMode == Q3DSSlide::Ping) {
        state = PlayerState::Paused;
    }

    // There are only two valid states after a slide finishes; paused or playing
    Q_ASSERT(state != PlayerState::Stopped);

    setInternalState(state);
}

void Q3DSSlidePlayerNg::evaluateDynamicObjectVisibility(Q3DSGraphObject *obj)
{
    bool parentVisible = true;
    // If this is a component player, then check if the component is visible.
    if (m_type == Q3DSSlidePlayerNg::PlayerType::Component) {
        // Since we can be called from another slide, check if there's a pending visibility
        // update for us.
        const auto foundIt = m_sceneManager->m_pendingObjectVisibility.constFind(m_component);
        if (foundIt != m_sceneManager->m_pendingObjectVisibility.cend())
            parentVisible = foundIt.value();
        else
            parentVisible = (m_component->attached()->visibilityTag == Q3DSGraphObjectAttached::Visible);
    }

    float time = -1.0f;
    Q3DSSlide *slide = m_data.slideDeck->currentSlide();
    if (slide && (slide->objects().contains(obj) || static_cast<Q3DSSlide *>(slide->parent())->objects().contains(obj)))
        time = position();

    setObjectVisibility(obj, parentVisible, true, time);
}

void Q3DSSlidePlayerNg::objectAboutToBeAddedToScene(Q3DSGraphObject *obj)
{
    evaluateDynamicObjectVisibility(obj);
}

void Q3DSSlidePlayerNg::objectAboutToBeRemovedFromScene(Q3DSGraphObject *obj)
{
    evaluateDynamicObjectVisibility(obj);
}

void Q3DSSlidePlayerNg::objectAddedToSlide(Q3DSGraphObject *obj, Q3DSSlide *slide)
{
    qDebug(lcSlidePlayer) << "Dyn.added object" << obj->id() << "to slide" << slide->id();
    evaluateDynamicObjectVisibility(obj);
    Q3DSAnimationManagerNg::addObject(slide, obj);
}

void Q3DSSlidePlayerNg::objectRemovedFromSlide(Q3DSGraphObject *obj, Q3DSSlide *slide)
{
    qDebug(lcSlidePlayer) << "Dyn.removed object" << obj->id() << "from slide" << slide->id();
    evaluateDynamicObjectVisibility(obj);
    Q3DSAnimationManagerNg::removeObject(slide, obj);
}

float Q3DSSlidePlayerNg::playbackRate() const
{
    return m_data.playbackRate;
}

QT_END_NAMESPACE
