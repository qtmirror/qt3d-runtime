CXX_MODULE = qml
TARGET = q3dsstudio3dplugin
TARGETPATH = QtStudio3D.2
IMPORT_VERSION = 2.0

QT += qml quick 3drender-private 3dstudioruntime2-private

include($$PWD/designer/designer.pri)

SOURCES += \
    plugin.cpp \
    q3dsstudio3ditem.cpp \
    q3dsstudio3drenderer.cpp \
    q3dspresentationitem.cpp \
    q3dssubpresentationsettings.cpp \
    q3dsstudio3dview.cpp

HEADERS += \
    q3dsstudio3ditem_p.h \
    q3dsstudio3drenderer_p.h \
    q3dspresentationitem_p.h \
    q3dssubpresentationsettings_p.h \
    q3dsstudio3dview_p.h \
    q3dsstudio3dviewdesc_p.h

OTHER_FILES += \
    qmldir

load(qml_plugin)
