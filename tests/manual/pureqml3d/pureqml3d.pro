TARGET = pureqml3d

QT += quick 3dstudioruntime2

SOURCES += \
    main.cpp

RESOURCES += pureqml3d.qrc

OTHER_FILES += \
    main.qml
