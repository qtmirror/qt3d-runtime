/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 2.2
import QtStudio3D 2.2

Rectangle {
    id: root
    color: "lightGray"

    Studio3D {
        id: s3d
        Presentation {
            id: pres
            source: "qrc:/manylayers.uip"
            profilingEnabled: true
        }
        property int layerCount: 3 // Layer1, Layer2, Layer3
    }

    ListView {
        anchors.fill: parent

        model: 32
        delegate: Rectangle {
            width: parent.width
            height: 100
            color: index % 2 ? "steelblue" : "lightsteelblue"
            Rectangle {
                border.color: "gray"
                border.width: 2
                radius: 8
                color: "transparent"
                anchors.centerIn: parent
                width: parent.width / 2
                height: parent.height - 10
                View3D {
                    id: view3D
                    anchors.fill: parent
                    engine: s3d
                    source: "Layer" + ((index % s3d.layerCount) + 1)
                }
            }
            Text {
                anchors.verticalCenter: parent.verticalCenter
                color: "green"
                text: "3D layer '" + view3D.source + "'"
                font.bold: true
            }
        }
    }

    Button {
        id: profBtn
        text: "Profile UI"
        // we use a Studio3DProfiler item so toggle that instead of pres.profileUiVisible
        onClicked: quickBasedProfileUI.visible = !quickBasedProfileUI.visible
        focusPolicy: Qt.NoFocus
    }

    // This example uses Qt Quick-based debug views because rendering them with
    // Qt 3D is not feasible anymore in the separated views model (as Qt 3D is
    // not responsible for the final composed image).
    Studio3DProfiler {
        id: quickBasedProfileUI
        visible: false
        focus: true
        anchors.fill: parent
        anchors.topMargin: 40 // make sure we do not cover the top area with the button
    }
}
