/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:FDL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU Free Documentation License Usage
** Alternatively, this file may be used under the terms of the GNU Free
** Documentation License version 1.3 as published by the Free Software
** Foundation and appearing in the file included in the packaging of
** this file. Please review the following information to ensure
** the GNU Free Documentation License version 1.3 requirements
** will be met: https://www.gnu.org/licenses/fdl-1.3.html.
** $QT_END_LICENSE$
**
****************************************************************************/

/*!
    \example scenemanip
    \title Qt 3D Studio Runtime: Dynamic Scene Manipulation C++ Example
    \ingroup qt3dstudioruntime2-examples-cpp
    \brief Demonstrates using the private C++ APIs to change the scene at run time.

    \image scenemanip.png

    \e {This example demonstrates basic usage of the Qt 3D Studio runtime's
    private C++ APIs in order to change the scene at run time.}

    \include examples-run.qdocinc

    This example uses the private C++ APIs from the Qt 3D Studio Runtime in
    order to programatically create models and materials in a Qt 3D Studio
    scene loaded from a \c{.uip} file into a Studio3D item inside a Qt Quick
    scene. It also demonstrates connecting QML and C++ logic to events that
    occur when an object is picked in the 3D scene.
*/
