CONFIG += c++11
QT += widgets qml quick 3dstudioruntime2

target.path = $$[QT_INSTALL_EXAMPLES]/3dstudioruntime2/$$TARGET
INSTALLS += target

SOURCES += \
        main.cpp

RESOURCES += \
    res.qrc
